package com.treslow.mssprint.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class SprintEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long idSprint;

    @Column
    private String sprintName;

    @Column
    private Date startDate;

    @Column
    private Date endDate;

    public long getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(long idSprint) {
        this.idSprint = idSprint;
    }

    public String getSprintName() {
        return sprintName;
    }

    public void setSprintName(String sprintName) {
        this.sprintName = sprintName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
