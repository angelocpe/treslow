package com.treslow.mssprint.repository;

import com.treslow.mssprint.entity.SprintEntity;
import org.springframework.data.repository.CrudRepository;

public interface SprintRepository extends CrudRepository<SprintEntity,Long > {
}
