package com.treslow.mssprint.controller;

import com.treslow.mssprint.config.Router;
import com.treslow.mssprint.service.SprintService;
import com.treslow.sprint_public.DTO.SprintDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class SprintController {
    private final static Logger LOG = LoggerFactory.getLogger(SprintController.class);

    @Autowired
    SprintService sprintService;

    @RequestMapping(method= RequestMethod.GET, value= Router.SPRINTS_URL)
    public List<SprintDTO> getAllSprints(){
        return sprintService.getAllSprints();
    }

    @RequestMapping(method= RequestMethod.GET, value=Router.SPRINT_URL)
    public ResponseEntity<SprintDTO> getSprint(@PathVariable Long id){
        SprintDTO sprintDTO
                = sprintService.getSprint(id);
        if (sprintDTO
                != null) {
            return new ResponseEntity<SprintDTO>(sprintDTO
                    , HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Sprint id:" + id + ", not found ", null);
        }
    }

    @RequestMapping(method=RequestMethod.POST,value=Router.POST_SPRINT_URL)
    public ResponseEntity<SprintDTO> addSprint(@RequestBody SprintDTO sprintDTO) {
        SprintDTO sprint = sprintService.addSprint(sprintDTO
        );
        if (sprint != null) {
            return new ResponseEntity<SprintDTO>(sprint
                    , HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sprint" + sprintDTO
                    + ", could not be inserted", null);
        }
    }
    @RequestMapping(method=RequestMethod.PUT,value=Router.SPRINT_URL)
    public ResponseEntity<SprintDTO> updateSprint(@PathVariable Long id, @RequestBody SprintDTO sprintDTO
    ) {
        SprintDTO sprint = sprintService.updateSprint( id,sprintDTO
        );
        if (sprint != null) {
            return new ResponseEntity<SprintDTO>(sprint, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sprint:" + sprintDTO
                    + ", could not be updated", null);
        }
    }
    @RequestMapping(method=RequestMethod.DELETE,value=Router.SPRINT_URL)
    public void deleteSprint(@PathVariable Long id) {
        sprintService.deleteSprint(id);
    }

}
