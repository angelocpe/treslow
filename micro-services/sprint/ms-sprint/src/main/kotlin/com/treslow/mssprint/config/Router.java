package com.treslow.mssprint.config;

public class Router {
    public static final String API_START  = "/sprint-service";
    public static final String SPRINTS_URL = API_START + "/sprints";
    public static final String SPRINT_URL = API_START +"/sprint/{id}";
    public static final String POST_SPRINT_URL = API_START +"/sprint";

}
