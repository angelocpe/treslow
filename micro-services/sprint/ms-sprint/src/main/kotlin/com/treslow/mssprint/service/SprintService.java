package com.treslow.mssprint.service;

import com.treslow.mssprint.entity.SprintEntity;
import com.treslow.mssprint.repository.SprintRepository;
import com.treslow.sprint_public.DTO.SprintDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service of Sprint
 */
@Service
public class SprintService {
    @Autowired
    ModelMapper mapper;
    private final SprintRepository sprintRepository;

    /**
     * Constructor
     * @param sprintRepository
     */
    public SprintService(SprintRepository sprintRepository) {
        this.sprintRepository = sprintRepository;
    }

    /**
     * Delete Sprint given its id
     * @param id
     */
    public void deleteSprint(Long id) {
        sprintRepository.deleteById(id);
    }

    /**
     * Update SprintDTO given its id and its new data (SprintDTO)
     * @param id
     * @param sprintDTO
     * @return SprintDTO updated, null if the Sprint doesn't already exist
     */
    public SprintDTO updateSprint(Long id, SprintDTO sprintDTO) {
        if (id != sprintDTO.getIdSprint()) {
            return null;
        } else if (!sprintRepository.existsById(id)) {
            return null;
        } else {
            SprintEntity entity = mapper.map(sprintDTO, SprintEntity.class);
            SprintEntity sprintDb = sprintRepository.save(entity);
            return mapper.map(sprintDb, SprintDTO.class);
        }
    }

    /**
     * Add Sprint to db
     * @param sprintDTO
     * @return SprintDTO added
     */
    public SprintDTO addSprint(SprintDTO sprintDTO) {
        SprintEntity sprintEntity = sprintRepository.save(mapper.map(sprintDTO, SprintEntity.class));
        return mapper.map(sprintEntity, SprintDTO.class);
    }

    /**
     * Get Sprint given its id
     * @param id
     * @return SprintDTO or null if Sprint doesn't exist
     */
    public SprintDTO getSprint(Long id) {
        Optional<SprintEntity> sprintEntity = sprintRepository.findById(id);
        return sprintEntity.map(entity -> mapper.map(entity, SprintDTO.class)).orElse(null);
    }

    /**
     * Get all Sprints stored in db
     * @return List of SprintDTO
     */
    public List<SprintDTO> getAllSprints() {
        List<SprintEntity> sprintEntities = new ArrayList<>();
        sprintRepository.findAll().forEach(sprintEntities::add);
        return sprintEntities
                .stream()
                .map(entry -> mapper.map(entry, SprintDTO.class))
                .collect(Collectors.toList());
    }
}
