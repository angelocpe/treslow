package com.treslow.mssprint

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import org.modelmapper.ModelMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication

@OpenAPIDefinition(info = Info(title = "Sprint Rest Api", version = "1.0", description = "Information about the Sprint APi"))

class MsSprintApplication

fun main(args: Array<String>) {
	runApplication<MsSprintApplication>(*args)
}