package com.treslow.mssprint.security.config

import com.treslow.mssprint.security.AuthTokenFilter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter


@EnableWebSecurity
@Configuration
class SecurityConfig @Autowired constructor(
    val authTokenFilter: AuthTokenFilter
) {

    @Bean
    @Throws(Exception::class)
    fun filterChain(http: HttpSecurity): SecurityFilterChain? {
        http
            .httpBasic().disable()
            .csrf().disable()
            .cors().and()
            .authorizeRequests()
            .anyRequest()
            .permitAll()
            .and()
            .httpBasic()


        http
            .addFilterBefore(authTokenFilter, WebAsyncManagerIntegrationFilter::class.java)
        return http.build()
    }
}