package com.treslow.mssprint.service;

import com.treslow.mssprint.entity.SprintEntity;
import com.treslow.mssprint.repository.SprintRepository;
import com.treslow.sprint_public.DTO.SprintDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
class SprintServiceTest {

    @MockBean(name = "sprintService")
    private SprintService service;

    @MockBean(name="sprintRepository")
    private SprintRepository repository;

    @Test
    void deleteSprint() {
        SprintEntity listEntity = new SprintEntity();
        listEntity.setSprintName("Yerevan");
        repository.save(listEntity);
        service.deleteSprint(listEntity.getIdSprint());
        verify(service).deleteSprint(listEntity.getIdSprint());
        SprintDTO newSprint = service.getSprint(listEntity.getIdSprint());
        Assert.assertNull(newSprint);
    }

    @Test
    void updateSprint() {
        SprintDTO listEntity = new SprintDTO();
        listEntity.setSprintName("Yerevan");
        listEntity.setIdSprint(0);

        when(service.getSprint(listEntity.getIdSprint())).thenReturn(listEntity);

        SprintDTO newSprint = service.getSprint(listEntity.getIdSprint());
        Assert.assertEquals(listEntity, newSprint);

        //Change Name
        listEntity.setSprintName("Borowen");

        when(service.updateSprint(listEntity.getIdSprint(), listEntity)).thenReturn(listEntity);
        newSprint = service.updateSprint(listEntity.getIdSprint(), listEntity);
        Assert.assertEquals(listEntity, newSprint);

        //Change id
        listEntity.setIdSprint(2);

        when(service.updateSprint(listEntity.getIdSprint(), listEntity)).thenReturn(null);
        newSprint = service.updateSprint(listEntity.getIdSprint(), listEntity);
        Assert.assertNull(newSprint);
    }

    @Test
    void addSprint() {
        SprintDTO listEntity = new SprintDTO();
        listEntity.setSprintName("Yerevan");
        listEntity.setIdSprint(0);

        when(service.addSprint(listEntity)).thenReturn(listEntity);

        SprintDTO newSprint = service.addSprint(listEntity);
        Assert.assertEquals(listEntity, newSprint);
    }

    @Test
    void getSprint() {
        SprintDTO listEntity = new SprintDTO();
        listEntity.setSprintName("Yerevan");
        listEntity.setIdSprint(0);

        when(service.getSprint(listEntity.getIdSprint())).thenReturn(listEntity);

        SprintDTO newSprint= service.getSprint(listEntity.getIdSprint());
        Assert.assertEquals(listEntity, newSprint);

        SprintDTO newEmptySprint = service.getSprint((long) 2);
        Assert.assertNull(newEmptySprint);
    }

    @Test
    void getAllSprints() {
        SprintDTO listEntity = new SprintDTO();
        listEntity.setSprintName("Yerevan");
        List<SprintDTO> list = singletonList(listEntity);

        when(service.getAllSprints()).thenReturn(list);

        List<SprintDTO> newSprint= service.getAllSprints();
        Assert.assertEquals(list, newSprint);
    }
}