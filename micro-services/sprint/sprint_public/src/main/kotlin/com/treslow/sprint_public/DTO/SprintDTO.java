package com.treslow.sprint_public.DTO;

import java.util.Date;

public class SprintDTO {
    private long idSprint;

    private String sprintName;

    private Date startDate;

    private Date endDate;

    public long getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(long idSprint) {
        this.idSprint = idSprint;
    }

    public String getSprintName() {
        return sprintName;
    }

    public void setSprintName(String sprintName) {
        this.sprintName = sprintName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
