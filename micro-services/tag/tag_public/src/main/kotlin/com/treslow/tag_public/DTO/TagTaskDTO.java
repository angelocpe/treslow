package com.treslow.tag_public.DTO;

public class TagTaskDTO {
    private long idTask;

    private long idTag;

    public long getIdTask() {
        return idTask;
    }

    public void setIdTask(long idTask) {
        this.idTask = idTask;
    }

    public long getIdTag() {
        return idTag;
    }

    public void setIdTag(long idTag) {
        this.idTag = idTag;
    }
}
