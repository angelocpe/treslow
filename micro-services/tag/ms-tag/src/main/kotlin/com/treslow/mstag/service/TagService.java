package com.treslow.mstag.service;

import com.treslow.mstag.entity.TagEntity;
import com.treslow.mstag.entity.TagTaskEntity;
import com.treslow.mstag.repository.TagRepository;
import com.treslow.mstag.repository.TagTaskRepository;
import com.treslow.tag_public.DTO.TagDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Service of Tag
 */
@Service
public class TagService {
    @Autowired
    ModelMapper mapper;

    private final TagRepository tagRepository;
    private final TagTaskRepository tagTaskRepository;

    /**
     * Constructor
     * @param tagRepository
     * @param tagTaskRepository
     */
    public TagService(TagRepository tagRepository, TagTaskRepository tagTaskRepository) {
        this.tagRepository = tagRepository;
        this.tagTaskRepository = tagTaskRepository;
    }

    /**
     * Delete Tag by its id and its related Task
     * @param id
     * @param idTask
     */
    public void deleteTag(Long id, Long idTask) {
        TagTaskEntity tagTaskEntity = tagTaskRepository.findTagTaskEntityByIdTagAndIdTask(id, idTask);
        if(tagTaskEntity != null){
            tagTaskRepository.delete(tagTaskEntity);
        }
        ArrayList<TagTaskEntity> tagTaskEntities = tagTaskRepository.findAllByIdTag(id);
        if(!tagTaskEntities.isEmpty()){
            tagRepository.deleteById(id);
        }
    }

    /**
     * Update TagDTO given its id and its new data (TagDTO)
     * @param id
     * @param tagDTO
     * @return TagDTO updated, null if the Tag doesn't already exist
     */
    public TagDTO updateTag(Long id, TagDTO tagDTO) {
        if (id != tagDTO.getIdTag()) {
            return null;
        } else if (!tagRepository.existsById(id)) {
            return null;
        } else {
            TagEntity tag = mapper.map(tagDTO, TagEntity.class);
            TagEntity tagDb = tagRepository.save(tag);
            return mapper.map(tagDb, TagDTO.class);
        }
    }
    /**
     * Add TagDTO given its data (TagDTO)
     * @param tagDTO
     * @return TagDTO add
     */
    public TagDTO addTag(TagDTO tagDTO) {
        TagEntity tag = tagRepository.save(mapper.map(tagDTO, TagEntity.class));
        return mapper.map(tag, TagDTO.class);
    }

    /**
     * Get TagDTO given its id
     * @param id
     * @return TagDTO, null if it doesn't exist
     */
    public TagDTO getTag(Long id) {
        Optional<TagEntity> tagEntity = tagRepository.findById(id);
        return tagEntity.map(entity -> mapper.map(entity, TagDTO.class)).orElse(null);
    }

    /**
     * Get all the Tags for a certain Task given by its id
     * @param idTask
     * @return List of related TagDTO for the Task
     */
    public List<TagDTO> getTagsForTask(Long idTask){
        List<TagTaskEntity> tagTaskEntities = tagTaskRepository.findAllByIdTask(idTask);
        if(!tagTaskEntities.isEmpty()){
            List<TagEntity> tagEntities = new ArrayList<>();
            for( TagTaskEntity tagTaskEntity : tagTaskEntities){
                tagEntities.add(tagRepository.findById(tagTaskEntity.getIdTag()).get());
            }
            return tagEntities.stream().map(entry -> mapper.map(entry, TagDTO.class)).collect(Collectors.toList());
        }
        return null;
    }

    /**
     * Get all TagDTO saved in db
     * @return List of TagDTO
     */
    public List<TagDTO> getAllTags() {
        List<TagEntity> tagEntities = new ArrayList<>();
        tagRepository.findAll().forEach(tagEntities::add);
        return tagEntities
                .stream()
                .map(entry -> mapper.map(entry, TagDTO.class))
                .collect(Collectors.toList());
    }

    /**
     * Add the TagTask (link between Tag and Task)
     * @param idTag
     * @param idTask
     * @return TagTaskEntity
     */
    public TagTaskEntity addTagTask(long idTag, long idTask) {
        return tagTaskRepository.save(new TagTaskEntity(idTask, idTag));
    }

    /**
     * Get all Tags for a certain list of Tasks given by their ids
     * @param ids List of ids of Tasks
     * @return Map<Long, List<TagDTO>> map with key = id of task, value = List of related TagDTO
     */
    public Map<Long, List<TagDTO>> getTagsForTasks(List<Long> ids) {
        Map<Long, List<TagDTO>> map = new HashMap<>();
        for(Long id : ids){
            List<TagTaskEntity> tagTaskEntities = tagTaskRepository.findAllByIdTask(id);
            if(!tagTaskEntities.isEmpty()){
                List<TagEntity> tagEntities = new ArrayList<>();
                for(TagTaskEntity tagTaskEntity : tagTaskEntities){
                    Optional<TagEntity> tagEntity = tagRepository.findById(tagTaskEntity.getIdTag());
                    tagEntity.ifPresent(tagEntities::add);
                }
                 map.put(id, tagEntities.stream().map(entry -> mapper.map(entry, TagDTO.class)).collect(Collectors.toList()));
            }
        }
        return map;
    }
}
