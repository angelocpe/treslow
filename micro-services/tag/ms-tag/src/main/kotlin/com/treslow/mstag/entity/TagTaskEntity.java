package com.treslow.mstag.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity
@IdClass(TagTaskEntity.class)
public class TagTaskEntity implements Serializable {

    @Id
    private long idTask;

    @Id
    private long idTag;

    public TagTaskEntity(long idTask, long idTag) {
        this.idTask = idTask;
        this.idTag = idTag;
    }

    public TagTaskEntity() {

    }

    public long getIdTask() {
        return idTask;
    }

    public void setIdTask(long idTask) {
        this.idTask = idTask;
    }

    public long getIdTag() {
        return idTag;
    }

    public void setIdTag(long idTag) {
        this.idTag = idTag;
    }
}
