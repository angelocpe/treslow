package com.treslow.mstag.entity;

import javax.persistence.*;

@Entity
public class TagEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long idTag;

    @Column
    private String tagName;

    @Column
    private String color;

    public long getIdTag() {
        return idTag;
    }

    public void setIdTag(long idTag) {
        this.idTag = idTag;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
