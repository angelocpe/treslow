package com.treslow.mstag.repository;

import com.treslow.mstag.entity.TagEntity;
import org.springframework.data.repository.CrudRepository;


public interface TagRepository extends CrudRepository<TagEntity, Long> {
}
