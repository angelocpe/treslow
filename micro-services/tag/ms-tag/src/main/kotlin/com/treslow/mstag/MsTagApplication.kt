package com.treslow.mstag

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import org.modelmapper.ModelMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication

@OpenAPIDefinition(info = Info(title = "Tag Rest Api", version = "1.0", description = "Information about the Tag APi"))

class MsTagApplication

fun main(args: Array<String>) {
	runApplication<MsTagApplication>(*args)
}

