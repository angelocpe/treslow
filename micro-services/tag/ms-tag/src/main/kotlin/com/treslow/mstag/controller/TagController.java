package com.treslow.mstag.controller;

import com.treslow.mstag.config.Router;
import com.treslow.mstag.service.TagService;
import com.treslow.tag_public.DTO.TagDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;


@RestController
public class TagController {
    private final static Logger LOG = LoggerFactory.getLogger(TagController.class);

    @Autowired
    TagService tagService;

    @RequestMapping(method= RequestMethod.GET, value=Router.API_START +"/tags")
    public List<TagDTO> getAllTags(){
        return tagService.getAllTags();
    }
    @RequestMapping(method= RequestMethod.GET, value=Router.API_START +"/tag/{id}")
    public ResponseEntity<TagDTO> getTag(@PathVariable Long id){
        TagDTO tagDTO = tagService.getTag(id);
        if (tagDTO != null) {
            return new ResponseEntity<TagDTO>(tagDTO, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag id:" + id + ", not found", null);
        }
    }
    @RequestMapping(method= RequestMethod.GET, value=Router.API_START +"/tags/task/{id}")
    public List<TagDTO> getAllTagsForTask(@PathVariable Long id){
        List<TagDTO> tagDTOs = tagService.getTagsForTask(id);
        if (tagDTOs != null) {
            return tagDTOs;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task for Tags id:" + id + ", not found", null);
        }
    }
    @RequestMapping(method=RequestMethod.POST,value=Router.API_START +"/tag/{idTask}")
    public ResponseEntity<TagDTO> addTag(@RequestBody TagDTO tagDTO, @PathVariable long idTask) {
        TagDTO tag = tagService.addTag(tagDTO);
        if (tag != null) {
            tagService.addTagTask(tag.getIdTag(), idTask);
            return new ResponseEntity<TagDTO>(tag, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tag:" + tagDTO + ", could not be inserted", null);
        }
    }
    @RequestMapping(method=RequestMethod.PUT,value=Router.API_START +"/tag/{id}")
    public ResponseEntity<TagDTO> updateTag(@PathVariable Long id,@RequestBody TagDTO tagDTO) {
        TagDTO tag = tagService.updateTag(id,tagDTO);
        if (tag != null) {
            return new ResponseEntity<TagDTO>(tag, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tag:" + tagDTO + ", could not be updated", null);
        }
    }
    @RequestMapping(method=RequestMethod.DELETE,value=Router.API_START +"/tag/{idTag}/{idTask}")
    public void deleteTag(@PathVariable Long idTag, @PathVariable Long idTask) {
        tagService.deleteTag(idTag, idTask);
    }

    @RequestMapping(method=RequestMethod.POST,value= Router.API_START +"/tags/tasks")
    public Map<Long, List<TagDTO>> getTagsForTasks(@RequestBody List<Long> ids){
       return tagService.getTagsForTasks(ids);
    }

}
