package com.treslow.mstag.repository;

import com.treslow.mstag.entity.TagTaskEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface TagTaskRepository extends CrudRepository<TagTaskEntity, TagTaskEntity> {
    void deleteAllByIdTask(Long idTask);
    ArrayList<TagTaskEntity> findAllByIdTask(Long idTask);

    void deleteAllByIdTag(Long idTag);
    ArrayList<TagTaskEntity> findAllByIdTag(Long idTag);

    TagTaskEntity findTagTaskEntityByIdTagAndIdTask(Long idTag, Long idTask);
    void deleteTagTaskEntityByIdTagAndIdTask(Long idTag, Long idTask);

}
