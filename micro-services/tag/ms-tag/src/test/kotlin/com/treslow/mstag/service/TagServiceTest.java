package com.treslow.mstag.service;

import com.treslow.mstag.entity.TagEntity;
import com.treslow.mstag.entity.TagTaskEntity;
import com.treslow.mstag.repository.TagRepository;
import com.treslow.tag_public.DTO.TagDTO;
import com.treslow.tag_public.DTO.TagTaskDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
class TagServiceTest {

    @MockBean(name = "tagService")
    private TagService service;

    @MockBean(name="tagRepository")
    private TagRepository repository;

    @Test
    void deleteTag() {
        TagEntity listEntity = new TagEntity();
        listEntity.setTagName("Yerevan");
        repository.save(listEntity);
        service.deleteTag(listEntity.getIdTag(), 1L);
        verify(service).deleteTag(listEntity.getIdTag(), 1L);
        TagDTO newTag = service.getTag(listEntity.getIdTag());
        Assert.assertNull(newTag);
    }

    @Test
    void updateTag() {
        TagDTO listEntity = new TagDTO();
        listEntity.setTagName("Yerevan");
        listEntity.setIdTag(0);

        when(service.getTag(listEntity.getIdTag())).thenReturn(listEntity);

        TagDTO newTag = service.getTag(listEntity.getIdTag());
        Assert.assertEquals(listEntity, newTag);

        //Change Name
        listEntity.setTagName("Borowen");

        when(service.updateTag(listEntity.getIdTag(), listEntity)).thenReturn(listEntity);
        newTag = service.updateTag(listEntity.getIdTag(), listEntity);
        Assert.assertEquals(listEntity, newTag);

        //Change id
        listEntity.setIdTag(2);

        when(service.updateTag(listEntity.getIdTag(), listEntity)).thenReturn(null);
        newTag = service.updateTag(listEntity.getIdTag(), listEntity);
        Assert.assertNull(newTag);
    }

    @Test
    void addTag() {
        TagDTO listEntity = new TagDTO();
        listEntity.setTagName("Yerevan");
        listEntity.setIdTag(0);

        when(service.addTag(listEntity)).thenReturn(listEntity);

        TagDTO newTag = service.addTag(listEntity);
        Assert.assertEquals(listEntity, newTag);
    }

    @Test
    void getTag() {
        TagDTO listEntity = new TagDTO();
        listEntity.setTagName("Yerevan");
        listEntity.setIdTag(0);

        when(service.getTag(listEntity.getIdTag())).thenReturn(listEntity);

        TagDTO newTag= service.getTag(listEntity.getIdTag());
        Assert.assertEquals(listEntity, newTag);

        TagDTO newEmptyTag = service.getTag((long) 2);
        Assert.assertNull(newEmptyTag);
    }

    @Test
    void getTagsForTask() {
        TagDTO tagDTO = new TagDTO();
        tagDTO.setTagName("Yerevan");
        tagDTO.setIdTag(1);
        TagTaskDTO taskDTO = new TagTaskDTO();
        taskDTO.setIdTask(1);
        taskDTO.setIdTag(1);
        List<TagDTO> list = singletonList(tagDTO);
        when(service.getTagsForTask(1L)).thenReturn(list);

        List<TagDTO> newTag= service.getTagsForTask(1L);
        Assert.assertEquals(list, newTag);
    }

    @Test
    void getAllTags() {
        TagDTO tagDTO = new TagDTO();
        tagDTO.setTagName("Yerevan");
        List<TagDTO> list = singletonList(tagDTO);

        when(service.getAllTags()).thenReturn(list);

        List<TagDTO> newTag= service.getAllTags();
        Assert.assertEquals(list, newTag);
    }

    @Test
    void addTagTask() {
        TagTaskEntity taskDTO = new TagTaskEntity();
        taskDTO.setIdTask(1);
        taskDTO.setIdTag(1);
        when(service.addTagTask(1L, 1L)).thenReturn(taskDTO);

        TagTaskEntity newTag= service.addTagTask(1L,1L);
        Assert.assertEquals(taskDTO, newTag);
    }
}