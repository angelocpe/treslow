package com.treslow.board_public;

import com.treslow.role_public.RoleDTO;
import com.treslow.user_public.UserDTO;

public class UserRoleDTO {
    private UserDTO userDTO;
    private RoleDTO roleDTO;

    public UserRoleDTO() {
    }

    public UserRoleDTO(UserDTO userDTO, RoleDTO roleDTO) {
        this.userDTO = userDTO;
        this.roleDTO = roleDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public RoleDTO getRoleDTO() {
        return roleDTO;
    }

    public void setRoleDTO(RoleDTO roleDTO) {
        this.roleDTO = roleDTO;
    }

    @Override
    public String toString() {
        return "UserRoleDTO{" +
                "userDTO=" + userDTO +
                ", roleDTO=" + roleDTO +
                '}';
    }
}
