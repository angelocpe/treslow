package com.treslow.board_public;

import com.treslow.list_public.DTO.ListDTO;

import java.util.ArrayList;
import java.util.List;

public class BoardDTO {
    private Long idBoard;
    private String boardName;
    private String background;
    private Long idAuthor;
    private List<UserRoleDTO> userRoles;

    private List<ListDTO> lists;

    public BoardDTO() {
    }

    public BoardDTO(Long idBoard, String boardName, String background, Long idAuthor, List<UserRoleDTO> userRoles) {
        this.idBoard = idBoard;
        this.boardName = boardName;
        this.background = background;
        this.idAuthor = idAuthor;
        this.userRoles = userRoles;
        this.lists = new ArrayList<>();
    }

    public BoardDTO(Long idBoard, String boardName, String background, Long idAuthor, List<UserRoleDTO> userRoles, List<ListDTO> lists) {
        this.idBoard = idBoard;
        this.boardName = boardName;
        this.background = background;
        this.idAuthor = idAuthor;
        this.userRoles = userRoles;
        this.lists = lists;
    }

    public Long getIdBoard() {
        return idBoard;
    }

    public void setIdBoard(Long idBoard) {
        this.idBoard = idBoard;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public Long getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(Long idAuthor) {
        this.idAuthor = idAuthor;
    }

    public List<UserRoleDTO> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRoleDTO> userRoles) {
        this.userRoles = userRoles;
    }

    public List<ListDTO> getLists() {
        return lists;
    }

    public void setLists(List<ListDTO> lists) {
        this.lists = lists;
    }

    @Override
    public String toString() {
        return "BoardDTO{" +
                "idBoard=" + idBoard +
                ", boardName='" + boardName + '\'' +
                ", background='" + background + '\'' +
                ", idAuthor=" + idAuthor +
                ", userRoles=" + userRoles +
                ", lists=" + lists +
                '}';
    }
}
