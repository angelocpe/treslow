package com.treslow.msboard

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MsBoardApplication

fun main(args: Array<String>) {
	runApplication<MsBoardApplication>(*args)
}
