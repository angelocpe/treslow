package com.treslow.msboard.entity

import javax.persistence.*

@Entity
@Table(name="board")
data class BoardEntity (
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    val idBoard: Long,
    val boardName: String,
    val background: String,
    var idAuthor: Long,
    @OneToMany(mappedBy = "boardEntity", cascade = [CascadeType.ALL])
    var boardUserRoleEntities: List<BoardUserRolesEntity>
)