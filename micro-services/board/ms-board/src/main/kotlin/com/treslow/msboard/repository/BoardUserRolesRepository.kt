package com.treslow.msboard.repository

import com.treslow.msboard.entity.BoardUserRolesEntity
import com.treslow.msboard.entity.CompositeKeyBoardUserRoles
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BoardUserRolesRepository : JpaRepository<BoardUserRolesEntity, CompositeKeyBoardUserRoles>