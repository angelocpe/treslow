package com.treslow.msboard.controller

import com.treslow.board_public.BoardDTO
import com.treslow.msboard.config.Router
import com.treslow.msboard.security.JwtUtils
import com.treslow.msboard.service.BoardServiceInterface
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
class BoardController @Autowired constructor(
    val boardServiceInterface: BoardServiceInterface,
    val jwtUtils: JwtUtils
){
    @GetMapping(Router.BOARDS_URL)
    fun getAllUserBoards(@RequestHeader("Authorization") authorization: String): List<BoardDTO> {
        val jwtToken = authorization.substring(7, authorization.length)
        val userId = jwtUtils.getUserIdFromToken(jwtToken)
        return boardServiceInterface.getAllUserBoards(jwtToken, userId)
    }

    @PostMapping(Router.BOARDS_URL)
    fun postBoard(
        @RequestHeader("Authorization") authorization: String,
        @RequestBody boardDTO: BoardDTO
    ): BoardDTO {
        val jwtToken = authorization.substring(7, authorization.length)
        val userId = jwtUtils.getUserIdFromToken(jwtToken)
        return boardServiceInterface.createBoard(jwtToken, userId, boardDTO)
    }

    @PutMapping(Router.BOARDS_URL)
    fun putBoard(
        @RequestHeader("Authorization") authorization: String,
        @RequestBody boardDTO: BoardDTO
    ): BoardDTO {
        val jwtToken = authorization.substring(7, authorization.length)
        val userId = jwtUtils.getUserIdFromToken(jwtToken)
        return boardServiceInterface.updateBoard(jwtToken, userId, boardDTO)
    }

    @GetMapping(Router.BOARD_URL)
    fun getBoard(
        @RequestHeader("Authorization") authorization: String,
        @PathVariable id: Long
    ): BoardDTO {
        val jwtToken = authorization.substring(7, authorization.length)
        return boardServiceInterface.getBoardById(jwtToken, id)
    }

    @DeleteMapping
    fun deleteBoardById(@RequestBody boardId: Long) = boardServiceInterface.deleteBoardById(boardId)
}