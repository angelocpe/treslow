package com.treslow.msboard.service

import com.treslow.board_public.BoardDTO
import com.treslow.board_public.UserRoleDTO
import com.treslow.list_public.DTO.ListDTO
import com.treslow.msboard.mapper.toDto
import com.treslow.msboard.mapper.toEntity
import com.treslow.msboard.repository.BoardRepository
import com.treslow.msboard.service.external.ListService
import com.treslow.msboard.service.external.RoleService
import com.treslow.msboard.service.external.UserService
import com.treslow.role_public.BoardUserRoles
import com.treslow.role_public.RoleDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException
import org.springframework.stereotype.Service


@Service
class BoardServiceImpl @Autowired constructor(
    val boardRepository: BoardRepository,
    val userService: UserService,
    val roleService: RoleService,
    val listService: ListService
): BoardServiceInterface {

    /**
     * Return all users boards thank to user token which contains user id.
     * Retrieves the users contained in all boards and their roles.
     */
    override fun getAllUserBoards(token: String, userId: Long): List<BoardDTO> {
        val boardEntities = boardRepository.findAllUserBoardsByUserId(userId)
        val boardDTOs = mutableListOf<BoardDTO>()
        // Map with key = idBoard, value = Map with key = userId, value = roleId
        val mapBoardIdUserRoles = HashMap<Long, HashMap<Long, Long>>()
        boardEntities.map { boardEntity -> boardDTOs.add(boardEntity.toDto()) }
        for (boardEntity in boardEntities) {
            mapBoardIdUserRoles[boardEntity.idBoard] = hashMapOf()
            for (boardUserRole in boardEntity.boardUserRoleEntities) {
                mapBoardIdUserRoles[boardEntity.idBoard]?.put(boardUserRole.idUser, boardUserRole.idRole)
            }
        }

        // List containing all users in boards of user who's id is userId
        val userIds = hashSetOf<Long>()
        for(hash in mapBoardIdUserRoles.values) {
            for(userIdEntry in hash.keys) userIds.add(userIdEntry)
        }

        // Map with key = userId, value = UserDTO
        val userMap = userService.getUsersByIds(token, userIds.toList())

        // Map with key = roleId, value = RoleDTO
        val roleMap = roleService.getRolesMap(token)

        // Linking all UserDTOs and RoleDTOs to boards referencing them
        for(boardDTO in boardDTOs) {
            mapBoardIdUserRoles[boardDTO.idBoard]?.let {
                for (userIdEntry in it.keys) boardDTO.userRoles.add(
                    UserRoleDTO(
                        userMap?.get(userIdEntry),
                        roleMap?.get(it[userIdEntry])
                    )
                )
            }
        }
        return boardDTOs
    }

    /**
     * Get a board by board id.
     * Retrieves all users and teir roles in said board as well as all lists.
     */
    override fun getBoardById(token: String, boardId: Long): BoardDTO {
        val boardSearch = boardRepository.findById(boardId)
        if (boardSearch.isPresent) {
            val boardEntity = boardSearch.get()
            val userIdRoleIdMap = hashMapOf<Long, Long>()
            val userIds = boardEntity.boardUserRoleEntities.map { b -> b.idUser }

            boardEntity.boardUserRoleEntities.map { boardUserRolesEntity ->
                {
                    userIdRoleIdMap[boardUserRolesEntity.idUser] = boardUserRolesEntity.idRole
                }
            }
            // Map with key = userId, value = UserDTO
            val userMap = userService.getUsersByIds(token, userIds)

            // Map with key = roleId, value = RoleDTO
            val roleMap = roleService.getRolesMap(token)

            val boardDTO = boardEntity.toDto()
            boardDTO.userRoles = userIds.map { userId ->
                UserRoleDTO(
                    userMap?.get(userId),
                    roleMap?.get(userIdRoleIdMap[userId])
                )
            }

            boardDTO.lists = listService.getListsForBoard(token, boardDTO.idBoard)
            return boardDTO
        }
        throw NotFoundException()
    }

    /**
     * Creates a board with an empty list named 'Backlog'.
     */
    override fun createBoard(token: String, userId: Long, board: BoardDTO): BoardDTO {
        val boardEntity = board.toEntity()
        boardEntity.boardUserRoleEntities = arrayListOf()
        boardEntity.idAuthor = userId
        var boardDTO = boardRepository.save(boardEntity).toDto()

        boardDTO.userRoles = board.userRoles

        val userMap = userService.getUsersByIds(token, arrayListOf(userId))
        val rolesMap = roleService.getRolesMap(token)

        var roleAdministrator: RoleDTO? = null

        rolesMap?.values?.let {
            for (role in it) {
                if (role.name == BoardUserRoles.ROLE_ADMINISTRATOR) {
                    roleAdministrator = role
                    break
                }
            }
        }

        roleAdministrator?.let {
            boardDTO.userRoles?.add(UserRoleDTO(userMap?.get(userId), it))
        }

        boardDTO = boardRepository.save(boardDTO.toEntity()).toDto()
        boardDTO.userRoles = board.userRoles

        val listDTO = ListDTO(
            "Backlog",
            boardDTO.idBoard,
            0,
            0,
            arrayListOf()
        )

        boardDTO.lists = arrayListOf(
            listService.addListToBoard(token, listDTO)
        )

        return boardDTO
    }

    /**
     * Update board attributes.
     */
    override fun updateBoard(token: String, userId: Long, board: BoardDTO): BoardDTO {
        val boardEntity = board.toEntity()
        boardEntity.boardUserRoleEntities = arrayListOf()
        var boardDTO = boardRepository.save(boardEntity).toDto()
        boardDTO.userRoles = board.userRoles
        boardDTO = boardRepository.save(boardDTO.toEntity()).toDto()
        boardDTO.userRoles = board.userRoles
        return boardDTO
    }

    /**
     * Deletes board by id.
     */
    override fun deleteBoardById(boardId: Long) {
        boardRepository.deleteById(boardId)
    }

}