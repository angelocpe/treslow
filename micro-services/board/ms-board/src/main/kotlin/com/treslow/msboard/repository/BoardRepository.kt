package com.treslow.msboard.repository

import com.treslow.msboard.entity.BoardEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface BoardRepository : JpaRepository<BoardEntity, Long> {
    @Query("SELECT b from BoardEntity b JOIN b.boardUserRoleEntities where b.id = :id")
    override fun findById(@Param("id") id: Long): Optional<BoardEntity>

    @Query("SELECT b from BoardEntity b JOIN b.boardUserRoleEntities bur where bur.idUser = :userId ")
    fun findAllUserBoardsByUserId(@Param("userId") userId: Long): List<BoardEntity>
}