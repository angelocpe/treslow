package com.treslow.msboard.service

import com.treslow.board_public.BoardDTO

interface BoardServiceInterface {
    fun getAllUserBoards(token: String, userId: Long): List<BoardDTO>
    fun getBoardById(token: String, boardId: Long): BoardDTO
    fun createBoard(token: String, userId: Long, board: BoardDTO): BoardDTO
    fun updateBoard(token: String, userId: Long, board: BoardDTO): BoardDTO
    fun deleteBoardById(boardId: Long)
}