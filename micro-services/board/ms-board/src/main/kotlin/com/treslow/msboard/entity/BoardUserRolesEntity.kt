package com.treslow.msboard.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "board_user_roles")
@IdClass(CompositeKeyBoardUserRoles::class)
data class BoardUserRolesEntity (
    @Id
    @Column(name = "id_board")
    val idBoard: Long? = null,
    @Id
    val idUser: Long,
    @Id
    val idRole: Long,

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name="id_board", insertable = false, updatable = false)
    var boardEntity: BoardEntity? = null
)