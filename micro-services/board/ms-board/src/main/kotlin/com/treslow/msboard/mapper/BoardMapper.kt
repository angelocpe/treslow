package com.treslow.msboard.mapper

import com.treslow.board_public.BoardDTO
import com.treslow.msboard.entity.BoardEntity
import com.treslow.msboard.entity.BoardUserRolesEntity

fun BoardEntity.toDto() = BoardDTO(idBoard, boardName, background, idAuthor, arrayListOf())
fun BoardDTO.toEntity(): BoardEntity {
    val entity = BoardEntity(idBoard, boardName, background, idAuthor, arrayListOf())
    entity.boardUserRoleEntities = userRoles.map { userRoleDTO -> BoardUserRolesEntity(idBoard, userRoleDTO.userDTO.id, userRoleDTO.roleDTO.id, entity) }
    return entity
}