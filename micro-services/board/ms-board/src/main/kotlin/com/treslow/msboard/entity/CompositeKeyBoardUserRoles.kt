package com.treslow.msboard.entity

import java.io.Serializable

data class CompositeKeyBoardUserRoles(
    val idBoard: Long? = null,
    val idUser: Long? = null,
    val idRole: Long? = null
): Serializable
