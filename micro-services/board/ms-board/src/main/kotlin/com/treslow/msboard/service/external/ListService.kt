package com.treslow.msboard.service.external

import com.treslow.list_public.DTO.ListDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

/**
 * Service to fetch data from list micro-service
 */
@Service
class ListService
constructor(
    @Value("\${list.api.url}")
    val LIST_API_URL: String,
    @Autowired
    val restTemplate: RestTemplate
) {
    /**
     * Fetches a list of all board's lists.
     */
    fun getListsForBoard(token: String, boardId: Long): ArrayList<ListDTO>? {
        val responseType = object : ParameterizedTypeReference<ArrayList<ListDTO>>() {}
        val httpHeaders = HttpHeaders()
        httpHeaders.contentType = MediaType.APPLICATION_JSON
        httpHeaders.set("Authorization", "Bearer $token")
        val httpEntity = HttpEntity<Void>(httpHeaders)
        val responseEntity = restTemplate.exchange(
            "$LIST_API_URL/lists/boards/${boardId}",
            HttpMethod.GET,
            httpEntity,
            responseType
        )
        if (responseEntity.statusCode == HttpStatus.OK) {
            return responseEntity.body
        }
        return null
    }

    /**
     * Adds a list to a board.
     */
    fun addListToBoard(token: String, listDTO: ListDTO): ListDTO? {
        val httpHeaders = HttpHeaders()
        httpHeaders.contentType = MediaType.APPLICATION_JSON
        httpHeaders.set("Authorization", "Bearer $token")
        val httpEntity = HttpEntity<ListDTO>(listDTO, httpHeaders)
        val responseEntity = restTemplate.exchange(
            "$LIST_API_URL/list",
            HttpMethod.POST,
            httpEntity,
            ListDTO::class.java
        )
        if (responseEntity.statusCode == HttpStatus.OK) {
            return responseEntity.body
        }
        return null
    }
}