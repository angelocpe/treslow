package com.treslow.msboard.service.external

import com.treslow.role_public.RoleDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

/**
 * Service to fetch data from role micro-service
 */
@Service
class RoleService
constructor(
    @Value("\${role.api.url}")
    val ROLE_API_URL: String,
    @Autowired
    val restTemplate: RestTemplate
) {
    /**
     * Fetches a map of all roles. Key is roleId, value is RoleDTO
     */
    fun getRolesMap(token: String): Map<Long, RoleDTO>? {
        val responseType = object : ParameterizedTypeReference<HashMap<Long, RoleDTO>>() {}
        val httpHeaders = HttpHeaders()
        httpHeaders.contentType = MediaType.APPLICATION_JSON
        httpHeaders.set("Authorization", "Bearer $token")
        val httpEntity = HttpEntity<Void>(httpHeaders)
        val responseEntity = restTemplate.exchange(
            "$ROLE_API_URL/roles_map",
            HttpMethod.GET,
            httpEntity,
            responseType
        )
        if(responseEntity.statusCode == HttpStatus.OK) {
            return responseEntity.body
        }
        return null
    }
}