package com.treslow.msboard.config

object Router {
    const val API_START = "/board-service"
    const val BOARDS_URL = "$API_START/boards"
    const val BOARD_URL = "$API_START/board/{id}"
}