package com.treslow.msboard.service.external

import com.treslow.user_public.UserDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

/**
 * Service to fetch data from user micro-service
 */
@Service
class UserService
constructor(
    @Value("\${user.api.url}")
    val USER_API_URL: String,
    @Autowired
    val restTemplate: RestTemplate
) {
    /**
     * Fetches a map of all users. Key is userId, value is UserDTO
     */
    fun getUsersByIds(token: String, userIds: List<Long>): Map<Long, UserDTO>? {
        val responseType = object : ParameterizedTypeReference<HashMap<Long, UserDTO>>() {}
        val httpHeaders = HttpHeaders()
        httpHeaders.contentType = MediaType.APPLICATION_JSON
        httpHeaders.set("Authorization", "Bearer $token")
        val httpEntity = HttpEntity<List<Long>>(userIds, httpHeaders)
        val responseEntity = restTemplate.exchange(
            "$USER_API_URL/users_ids",
            HttpMethod.POST,
            httpEntity,
            responseType
        )
        if(responseEntity.statusCode == HttpStatus.OK) {
            return responseEntity.body
        }
        return null
    }
}