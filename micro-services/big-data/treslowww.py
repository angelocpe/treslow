##### L'équipe Trèslowww #####


from pathlib import PureWindowsPath
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
import os
import csv
import sqlite3


#On init le fichier qu'on va agrémenter si il existe pas
if not os.path.isfile('dataset/issues.csv'):
    with open('dataset/issues.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['issuekey', 'title', 'description', 'storypoint'])

# set files path
datacsv1 = 'dataset/bamboo.csv'
datacsv2 = 'dataset/clover.csv'
datacsv3 = 'dataset/datamanagement.csv'
datacsv4 = 'dataset/mesos.csv'

datacsv5 = 'dataset/appceleratorstudio.csv'
datacsv6 = 'dataset/duracloud.csv'
datacsv7 = 'dataset/jirasoftware.csv'
datacsv8 = 'dataset/moodle.csv'
datacsv9 = 'dataset/mule.csv'
datacsv10 = 'dataset/mulestudio.csv'


datacsv11 = 'dataset/springxd.csv'
datacsv12 = 'dataset/talenddataquality.csv'
datacsv13 = 'dataset/talendesb.csv'
datacsv14 = 'dataset/titanium.csv'
datacsv15 = 'dataset/usergrid.csv'
datacsv16 = 'dataset/issues.csv'

print("*** Merging multiple csv files into a single pandas dataframe ***")

# Gestion des erreurs
try:
    # Charger le dataset
    descriptions = pd.concat(
        map(pd.read_csv, [datacsv1, datacsv2, datacsv3, datacsv4, datacsv5, datacsv6, datacsv7, datacsv8,datacsv9,datacsv10,datacsv11,datacsv12,datacsv13,datacsv14,datacsv15,datacsv16]), ignore_index=True)
    descriptions = descriptions.dropna()

except FileNotFoundError as e:
    print(f"Error: One or more of the CSV files could not be found. Please check the file names and paths")
    print(e)
    exit()

except pd.errors.EmptyDataError as e:
    print(f"Error: One or more of the CSV files is empty or contains only NaN values")
    print(e)
    exit()

except Exception as e:
    print("Unexpected error:", e)
    exit()

print("Data loaded successfully!")


dataframe_refus = descriptions


descriptions = descriptions.dropna()
# Sélectionner la colonne "description" et enlever les lignes vides
dataframe_test = descriptions["description"].dropna()


# Créer un vecteur de features en utilisant TfidfVectorizer
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(dataframe_test)

# Entraîner un modèle KMeans avec 20 clusters (Vitesse d'éxecution du code optimale pour ne pas pénaliser le reste du backend)
kmeans = KMeans(n_clusters=20)
kmeans.fit(X)
# %store kmeans

# Prédire les clusters pour chaque description
predictions = kmeans.predict(X)

# Ajouter les prédictions au DataFrame original
descriptions["cluster"] = predictions

# Grouper les données par cluster
grouped_data = descriptions.groupby("cluster").apply(lambda x: x)


grouped_data_mean = descriptions.groupby("cluster")["storypoint"].mean()


#On fait le test pour la phrase "as a User I want to click"
sentence = ["as a User I want to click"]

X_new = vectorizer.transform(sentence)
prediction = kmeans.predict(X_new)
print("la phrase:"+sentence[0] + "est dans la catégorie:"+str(prediction[0]) + "avec la moyenne :" +str(grouped_data_mean[prediction[0]]))


#Récupération des thèmes (mots les plus importants) (trop gourmand en ressource pour google collab)

#import numpy as np
# Utiliser les prédictions pour sélectionner les descriptions de chaque cluster
#cluster_data = [X[np.where(predictions == i)].toarray() for i in range(kmeans.n_clusters)]

# Obtenir les mots les plus importants pour chaque cluster
#feature_names = vectorizer.get_feature_names()
#for i in range(kmeans.n_clusters):
    #cluster = X[np.where(predictions == i)].toarray()
    #word_count = np.sum(cluster, axis=0)
    #top_words_index = np.argsort(-word_count)
    #top_words = [feature_names[index] for index in top_words_index[:20]]
    #print("Thème du cluster {}: {}".format(i, ' '.join(top_words)))


#Gestion de BDD

# Connect to or create a new database
conn = sqlite3.connect('mydatabase.db')

# Create a new table
conn.execute('''CREATE TABLE IF NOT EXISTS issues
             (issuekey TEXT, title TEXT, description TEXT, storypoint INTEGER)''')


# Commit changes and close the connection
conn.commit()
conn.close()

def insertbdd(issuekey,title,description,storypoint):
    # Connect to the database
    conn = sqlite3.connect('mydatabase.db')
    cursor = conn.cursor()

    # Insert the values into the table
    cursor.execute("INSERT INTO issues (issuekey, title, description, storypoint) VALUES (?, ?, ?, ?)", (issuekey, title, description, storypoint))
    conn.commit()

    # Write the new row to the CSV file
    with open('issues.csv', 'a', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow([issuekey, title, description, storypoint])

    # Close the connection
    conn.close()





##########################################################################
#                               FLASK                                    # 
##########################################################################

from flask import Flask, render_template, request
app = Flask(__name__)

#Import des fichiers csv pour tenir compte des données entrées par l'utilisateur
descriptions = pd.concat(
   map(pd.read_csv, [datacsv1, datacsv2, datacsv3, datacsv4, datacsv5, datacsv6, datacsv7, datacsv8,datacsv9,datacsv10,datacsv11,datacsv12,datacsv13,datacsv14,datacsv15,datacsv16]), ignore_index=True)

@app.route("/")
def home():
    return "Hello World"

#Prédiction d'une phrase donnée à l'utilisateur, retourne un str au front avec la valeur du storypoint
@app.route("/big-data-service/prediction", methods=["POST"])
def predict():
    json_data = request.get_json()
    sentence = json_data["sentence"]
    # Use the `sentence` variable to make a prediction
    X_new = vectorizer.transform([sentence])
    prediction = kmeans.predict(X_new)
    insertbdd("ABC-1234","TEST",sentence,int(prediction[0]))
    retour = ("la phrase:"+sentence + "est dans la catégorie:"+str(prediction[0]) + "avec la moyenne :" +str(grouped_data_mean[prediction[0]]))
    return str(grouped_data_mean[prediction[0]])

#L'utilisateur dit non, et prédiction avec un nouveau N
#Route pas relié au front, disponible dans de futurs sprints
@app.route("/refus/<sentence>", methods=["GET"])
def refus(sentence):
    vectorizer = TfidfVectorizer()
    dataframe_refus = descriptions.dropna()
    dataframe_test_refus = dataframe_refus["description"].dropna()
    X_refus = vectorizer.fit_transform(dataframe_test_refus)
    kmeans = KMeans(n_clusters=60)
    kmeans.fit(X_refus)
    predictions = kmeans.predict(X_refus)
    dataframe_refus.loc[:,'cluster'] = predictions
    grouped_data = dataframe_refus.groupby("cluster").apply(lambda x: x)
    grouped_data_mean = dataframe_refus.groupby("cluster")["storypoint"].mean()
    X_new = vectorizer.transform([sentence])
    prediction = kmeans.predict(X_new)
    return "la phrase:"+sentence + "est dans la catégorie:"+str(prediction[0]) + "avec la moyenne :" +str(grouped_data_mean[prediction[0]])

@app.route("/dataset", methods=["GET"])
def dataset():
  return descriptions.to_dict()


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)

