package com.treslow.list_public.DTO;

import com.treslow.task_public.TaskDTO;

import java.util.List;

public class ListDTO {


    private String listName;

    private long idBoard;

    private int position;
    private long idList;

    private List<TaskDTO> tasks;

    public List<TaskDTO> getTasks() {
        return tasks;
    }

    public ListDTO() {
    }

    public ListDTO(String listName, long idBoard, int position, long idList, List<TaskDTO> tasks) {
        this.listName = listName;
        this.idBoard = idBoard;
        this.position = position;
        this.idList = idList;
        this.tasks = tasks;
    }

    public void setTasks(List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

    public long getIdList() {
        return idList;
    }

    public void setIdList(long idList) {
        this.idList = idList;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public long getIdBoard() {
        return idBoard;
    }

    public void setIdBoard(long idBoard) {
        this.idBoard = idBoard;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "ListDTO{" +
                "listName='" + listName + '\'' +
                ", idBoard=" + idBoard +
                ", position=" + position +
                ", idList=" + idList +
                ", tasks=" + tasks +
                '}';
    }
}
