package com.treslow.mslist.service;

import com.treslow.list_public.DTO.ListDTO;
import com.treslow.mslist.entity.ListEntity;
import com.treslow.mslist.repository.ListRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
class ListServiceTest {
    @MockBean(name = "listService")
    private ListService service;

    @MockBean(name="listRepository")
    private ListRepository repository;


    @Test
    void getAllLists()  {
        ListDTO listEntity = new ListDTO();
        listEntity.setListName("Yerevan");
        List<ListDTO> list = singletonList(listEntity);

        when(service.getAllLists()).thenReturn(list);

        List<ListDTO> newList= service.getAllLists();
        Assert.assertEquals(list, newList);

    }

    @Test
    void getList() {
        ListDTO listEntity = new ListDTO();
        listEntity.setListName("Yerevan");
        listEntity.setIdList(0);

        when(service.getList(listEntity.getIdList())).thenReturn(listEntity);

        ListDTO newList= service.getList(listEntity.getIdList());
        Assert.assertEquals(listEntity, newList);

        ListDTO newEmptyList = service.getList((long) 2);
        Assert.assertNull(newEmptyList);
    }

    @Test
    void updateList() {
        ListDTO listEntity = new ListDTO();
        listEntity.setListName("Yerevan");
        listEntity.setIdList(0);

        when(service.getList(listEntity.getIdList())).thenReturn(listEntity);

        ListDTO newList = service.getList(listEntity.getIdList());
        Assert.assertEquals(listEntity, newList);

        //Change Name
        listEntity.setListName("Borowen");

        when(service.updateList(listEntity.getIdList(), listEntity)).thenReturn(listEntity);
        newList = service.updateList(listEntity.getIdList(), listEntity);
        Assert.assertEquals(listEntity, newList);

        //Change id
        listEntity.setIdList(2);

        when(service.updateList(listEntity.getIdList(), listEntity)).thenReturn(null);
        newList = service.updateList(listEntity.getIdList(), listEntity);
        Assert.assertNull(newList);
    }

    @Test
    void addList() {
        ListDTO listEntity = new ListDTO();
        listEntity.setListName("Yerevan");
        listEntity.setIdList(0);

        when(service.addList(listEntity)).thenReturn(listEntity);

        ListDTO newList = service.addList(listEntity);
        Assert.assertEquals(listEntity, newList);
    }

    @Test
    void deleteList() {
        ListEntity listEntity = new ListEntity();
        listEntity.setListName("Yerevan");
        repository.save(listEntity);
        service.deleteList(listEntity.getIdList());
        verify(service).deleteList(listEntity.getIdList());
        ListDTO newList = service.getList(listEntity.getIdList());
        Assert.assertNull(newList);
    }
}