package com.treslow.mslist.service;

import com.treslow.list_public.DTO.ListDTO;
import com.treslow.mslist.entity.ListEntity;
import com.treslow.mslist.repository.ListRepository;
import com.treslow.mslist.service.external.TaskService;
import com.treslow.task_public.TaskDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Service of List
 */
@Service
public class ListService {

    @Autowired
    ModelMapper mapper;

    private final TaskService taskService;
    private final ListRepository listRepository;

    /**
     * Constructor
     * @param taskService
     * @param listRepository
     */
    public ListService(TaskService taskService, ListRepository listRepository) {
        this.taskService = taskService;
        this.listRepository = listRepository;
    }

    /**
     * Get all Lists stored in db
     * @return List of Lists
     */
    public List<ListDTO> getAllLists() {
        List<ListEntity> listEntities = new ArrayList<>();
        listRepository.findAll().forEach(listEntities::add);
        return listEntities
                .stream()
                .map(entry -> mapper.map(entry, ListDTO.class))
                .collect(Collectors.toList());
    }

    /**
     * Get List given its id
     * @param id
     * @return ListDTO
     */
    public ListDTO getList(Long id){
        Optional<ListEntity> listEntity = listRepository.findById(id);
        return listEntity.map(entity -> mapper.map(entity, ListDTO.class)).orElse(null);
    }

    /**
     * Update List given its id and its new data
     * @param id
     * @param listDTO new data
     * @return ListDTO updated or null if it doesn't exists
     */
    public ListDTO updateList(Long id, ListDTO listDTO){
        if (id != listDTO.getIdList()) {
            return null;
        } else if (!listRepository.existsById(id)) {
            return null;
        } else {
            ListEntity list = mapper.map(listDTO, ListEntity.class);
            ListEntity listBd = listRepository.save(list);
            return mapper.map(listBd, ListDTO.class);
        }
    }

    /**
     * Add List given its data
     * @param listDTO
     * @return ListDTO added o
     */
    public ListDTO addList(ListDTO listDTO){
        ListEntity list = listRepository.save(mapper.map(listDTO, ListEntity.class));
        return mapper.map(list, ListDTO.class);
    }

    /**
     * Delete List given its id
     * @param id
     */
    public void deleteList(Long id){
        listRepository.deleteById(id);
    }

    /**
     * Get All Lists related to one Board given its id
     * @param id of Board
     * @param token Security token
     * @return List of ListDTO related to Board
     */
    public List<ListDTO> getListsForBoard(Long id, String token) {

        //Get all Lists for each Board
        Map<Long, ListDTO> listMap = new HashMap<>();
        listRepository.findAllByIdBoard(id).forEach(list -> listMap.put(list.getIdList(), mapper.map(list, ListDTO.class)));

        //Get all Tasks for each Lists
        List<Long> listIds = new ArrayList<>();
        listMap.values().forEach(list -> listIds.add(list.getIdList()));
        Map<Long, List<TaskDTO>> tasksIdList = taskService.getTasksForLists(listIds, token);

        for(ListDTO list : listMap.values()){
            list.setTasks(tasksIdList.get(list.getIdList()));
        }

        //Map every information together
        List<ListDTO> listDTOList = new ArrayList<>(listMap.values());
        listDTOList.removeIf(list -> list.getIdBoard() != id);

        return listDTOList;
    }
}
