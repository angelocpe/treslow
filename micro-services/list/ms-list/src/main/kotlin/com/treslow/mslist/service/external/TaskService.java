package com.treslow.mslist.service.external;

import com.treslow.task_public.TaskDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
public class TaskService {
    @Autowired
    RestTemplate restTemplate;
    @Value("${task.api.url}")
    String TASK_API_URL;

    String GET_TASK_BY_LIST_URL = "/tasks/lists";

    public Map<Long, List<TaskDTO>> getTasksForLists(List<Long> listIds, String token) {
        ParameterizedTypeReference<Map<Long, List<TaskDTO>>> responseType = new ParameterizedTypeReference<>() {
        };
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("Authorization", "Bearer " + token);
        HttpEntity<List<Long>> httpEntity = new HttpEntity<>(listIds, httpHeaders);
        ResponseEntity<Map<Long, List<TaskDTO>>> responseEntity = restTemplate.exchange(
                TASK_API_URL + GET_TASK_BY_LIST_URL,
                HttpMethod.POST,
                httpEntity,
                responseType);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            return responseEntity.getBody();
        }
        return null;

    }
}
