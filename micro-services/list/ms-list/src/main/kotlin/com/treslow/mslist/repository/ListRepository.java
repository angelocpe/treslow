package com.treslow.mslist.repository;

import com.treslow.mslist.entity.ListEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ListRepository extends CrudRepository<ListEntity, Long> {
    List<ListEntity> findAllByIdBoard(Long idBoard);

}
