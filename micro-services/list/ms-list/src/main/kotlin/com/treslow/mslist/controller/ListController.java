package com.treslow.mslist.controller;

import com.treslow.list_public.DTO.ListDTO;
import com.treslow.mslist.config.Router;
import com.treslow.mslist.service.ListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class ListController {
    private final static Logger LOG = LoggerFactory.getLogger(ListController.class);

    @Autowired
    ListService listService;
    @RequestMapping(method= RequestMethod.GET, value=Router.API_START +"/lists")
    public List<ListDTO> getAllLists(){
        return listService.getAllLists();
    }
    @RequestMapping(method= RequestMethod.GET, value=Router.API_START +"/list/{id}")
    public ResponseEntity<ListDTO> getList(@PathVariable long id){
        ListDTO listDTO = listService.getList(id);
        if (listDTO != null) {
            return new ResponseEntity<ListDTO>(listDTO, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "List id:" + id + ", not found", null);
        }
    }
    @RequestMapping(method= RequestMethod.POST, value=Router.API_START +"/list")
    public ResponseEntity<ListDTO> addList(@RequestBody ListDTO listDTO) {
        ListDTO list = listService.addList(listDTO);
        if (list != null) {
            return new ResponseEntity<ListDTO>(list, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "List:" + listDTO + ", could not be inserted", null);
        }
    }
    @RequestMapping(method= RequestMethod.PUT, value=Router.API_START + "/list/{id}")
    public ResponseEntity<ListDTO> updateList(@PathVariable Long id, @RequestBody ListDTO listDTO) {
        ListDTO list = listService.updateList( id,listDTO);
        if (list != null) {
            return new ResponseEntity<ListDTO>(list, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "List:" + listDTO + ", could not be updated", null);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = Router.API_START + "/list/{id}")
    public void deleteList(@PathVariable Long id) {
        listService.deleteList(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = Router.API_START + "/lists/boards/{id}")
    public List<ListDTO> getListsForBoard(
            @RequestHeader("Authorization") String authorization,
            @PathVariable Long id
    ) {
        String jwtToken = authorization.substring(7);
        return listService.getListsForBoard(id, jwtToken);
    }
}
