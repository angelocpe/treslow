package com.treslow.mslist.entity;

import javax.persistence.*;

@Entity
public class ListEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long idList;

    @Column
    private String listName;

    @Column
    private long idBoard;

    @Column
    private int position;

    public long getIdList() {
        return idList;
    }

    public void setIdList(long idList) {
        this.idList = idList;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public long getIdBoard() {
        return idBoard;
    }

    public void setIdBoard(long idBoard) {
        this.idBoard = idBoard;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
