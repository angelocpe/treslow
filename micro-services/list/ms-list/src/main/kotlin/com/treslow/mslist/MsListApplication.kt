package com.treslow.mslist

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import org.modelmapper.ModelMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
@OpenAPIDefinition(info = Info(title = "List Rest Api", version = "1.0", description = "Information about the List APi"))
class MsListApplication

fun main(args: Array<String>) {
	runApplication<MsListApplication>(*args)
}
