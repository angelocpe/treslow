package com.treslow.role_public;

public class RoleDTO {
    private long id;
    private BoardUserRoles name;

    public RoleDTO() {
    }

    public RoleDTO(long id, BoardUserRoles name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BoardUserRoles getName() {
        return name;
    }

    public void setName(BoardUserRoles name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoleDTO{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
