package com.treslow.role_public;

public enum BoardUserRoles {
    ROLE_ADMINISTRATOR,
    ROLE_MODERATOR,
    ROLE_USER,
    ROLE_VIEWER,
}
