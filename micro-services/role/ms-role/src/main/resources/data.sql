INSERT INTO roles (id_role, name) values
    (1, 'ROLE_ADMINISTRATOR'),
    (2, 'ROLE_MODERATOR'),
    (3, 'ROLE_USER'),
    (4, 'ROLE_VIEWER');