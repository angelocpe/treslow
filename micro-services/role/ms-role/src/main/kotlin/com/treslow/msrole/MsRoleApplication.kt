package com.treslow.msrole


import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MsRoleApplication

fun main(args: Array<String>) {
	runApplication<MsRoleApplication>(*args)
}
