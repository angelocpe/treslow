package com.treslow.msrole.entity;

import com.treslow.role_public.BoardUserRoles;

import javax.persistence.*;

@Entity
@Table(name="roles")
public class RoleEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long idRole;

    @Enumerated(EnumType.STRING)
    private BoardUserRoles name;

    public RoleEntity(Long idRole, BoardUserRoles name) {
        this.idRole = idRole;
        this.name = name;
    }

    public RoleEntity() {

    }

    public long getIdRole() {
        return idRole;
    }

    public void setIdRole(Long idRole) {
        this.idRole = idRole;
    }

    public BoardUserRoles getName() {
        return name;
    }

    public void setName(BoardUserRoles name) {
        this.name = name;
    }
}
