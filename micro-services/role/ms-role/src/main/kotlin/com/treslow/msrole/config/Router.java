package com.treslow.msrole.config;

public class Router {

    public static final String API_START = "role-service";
    public static final String ROLES_URL = API_START + "/roles";
    public static final String ROLES_IDS_URL = API_START + "/roles_map";
}
