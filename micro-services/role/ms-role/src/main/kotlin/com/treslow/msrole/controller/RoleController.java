package com.treslow.msrole.controller;

import com.treslow.msrole.config.Router;
import com.treslow.msrole.service.RoleService;
import com.treslow.role_public.RoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class RoleController {
    final RoleService roleService;
    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping(Router.ROLES_URL)
    public List<RoleDTO> getAllRoles(){
        List<RoleDTO> roles = roleService.getAllRoles();
        return roleService.getAllRoles();
    }

    @GetMapping(Router.ROLES_IDS_URL)
    public Map<Long, RoleDTO> getAllRolesMap() {
        return roleService.getAllRolesMap();
    }
}
