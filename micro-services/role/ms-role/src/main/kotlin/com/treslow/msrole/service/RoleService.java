package com.treslow.msrole.service;

import com.treslow.msrole.entity.RoleEntity;
import com.treslow.msrole.repository.RoleRepository;
import com.treslow.role_public.RoleDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Service of Role
 */
@Service
public class RoleService {

    final ModelMapper mapper;
    private final RoleRepository roleRepository;

    /**
     * Constructor
     * @param roleRepository
     * @param mapper
     */
    @Autowired
    public RoleService(RoleRepository roleRepository, ModelMapper mapper) {
        this.roleRepository = roleRepository;
        this.mapper = mapper;
    }

    /**
     * Get All Roles stored in db
     * @return List of Roles
     */
    public List<RoleDTO> getAllRoles() {
        List<RoleEntity> roleEntities = new ArrayList<>();
        roleRepository.findAll().forEach(roleEntities::add);
        return roleEntities
                .stream()
                .map(entry -> mapper.map(entry, RoleDTO.class))
                .collect(Collectors.toList());
    }

    /**
     * Get all Roles
     * @return Map of key = id Role and value = RoleDTO
     */
    public Map<Long, RoleDTO> getAllRolesMap() {
        Map<Long, RoleDTO> roleDTOsMap = new HashMap<>();
        roleRepository.findAll().forEach(roleEntity -> roleDTOsMap.put(roleEntity.getIdRole(), mapper.map(roleEntity, RoleDTO.class)));
        return roleDTOsMap;
    }
}
