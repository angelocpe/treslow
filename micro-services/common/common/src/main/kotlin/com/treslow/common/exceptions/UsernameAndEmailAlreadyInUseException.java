package com.treslow.common.exceptions;

public class UsernameAndEmailAlreadyInUseException extends RuntimeException {
    public UsernameAndEmailAlreadyInUseException(String message) {
        super(message);
    }
}
