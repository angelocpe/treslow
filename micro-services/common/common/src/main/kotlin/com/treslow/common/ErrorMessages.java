package com.treslow.common;

import org.springframework.http.HttpStatus;

public enum ErrorMessages {
    EmailAlreadyInUse("Email is already used", HttpStatus.CONFLICT, 1L),
    PasswordNotMatch("Password does not match", HttpStatus.UNAUTHORIZED, 2L),
    UsernameAlreadyInUse("Username is already used", HttpStatus.CONFLICT, 3L),
    UserNotFound("User not found", HttpStatus.NOT_FOUND, 4L),
    UsernameAndEmailAlreadyInUse("Username and email are already used", HttpStatus.UNAUTHORIZED, 5L),

    RegistrationFailed("Registration failed", HttpStatus.INTERNAL_SERVER_ERROR, 6L),

    MalformedJwt("", HttpStatus.UNAUTHORIZED, 7L),

    ExpiredJwt("", HttpStatus.FORBIDDEN, 8L),

    UnsupportedJwt("", HttpStatus.FORBIDDEN, 9L),

    IllegalArgument("", HttpStatus.FORBIDDEN, 10L),

    Exception("", HttpStatus.INTERNAL_SERVER_ERROR, 11L),
    Unauthorized("Unauthorized", HttpStatus.FORBIDDEN, 12L),
    ;
    private final String message;
    private final HttpStatus status;
    private final Long code;

    ErrorMessages(String message, HttpStatus status, Long code) {
        this.message = message;
        this.status = status;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public Long getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "com.treslow.common.ErrorMessages{" +
                "message='" + message + '\'' +
                ", status=" + status +
                ", code=" + code +
                '}';
    }
}
