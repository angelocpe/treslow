package com.treslow.common;

import org.springframework.http.HttpStatus;

public class ErrorResponseDTO {
    private String message;
    private HttpStatus status;
    private long code;

    public ErrorResponseDTO(String message, HttpStatus status, long code) {
        this.message = message;
        this.status = status;
        this.code = code;
    }

    public ErrorResponseDTO() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "com.treslow.common.ErrorResponseDTO{" +
                "message='" + message + '\'' +
                ", status=" + status +
                ", code=" + code +
                '}';
    }
}
