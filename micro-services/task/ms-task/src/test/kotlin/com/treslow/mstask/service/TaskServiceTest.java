package com.treslow.mstask.service;

import com.treslow.mstask.entity.TaskEntity;
import com.treslow.mstask.repository.TaskRepository;
import com.treslow.task_public.TaskDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
class TaskServiceTest {
    @MockBean(name = "taskService")
    private TaskService service;

    @MockBean(name="taskRepository")
    private TaskRepository repository;

    @Test
    void deleteTask() {
        TaskEntity listEntity = new TaskEntity();
        listEntity.setTaskName("Yerevan");
        repository.save(listEntity);
        service.deleteTask(listEntity.getIdTask());
        verify(service).deleteTask(listEntity.getIdTask());
        TaskDTO newTask = service.getTask(listEntity.getIdTask());
        Assert.assertNull(newTask);
    }

    @Test
    void updateTask() {
        TaskDTO listEntity = new TaskDTO();
        listEntity.setTaskName("Yerevan");
        listEntity.setIdTask(0);

        when(service.getTask(listEntity.getIdTask())).thenReturn(listEntity);

        TaskDTO newTask = service.getTask(listEntity.getIdTask());
        Assert.assertEquals(listEntity, newTask);

        //Change Name
        listEntity.setTaskName("Borowen");

        when(service.updateTask(listEntity.getIdTask(), listEntity)).thenReturn(listEntity);
        newTask = service.updateTask(listEntity.getIdTask(), listEntity);
        Assert.assertEquals(listEntity, newTask);

        //Change id
        listEntity.setIdTask(2);

        when(service.updateTask(listEntity.getIdTask(), listEntity)).thenReturn(null);
        newTask = service.updateTask(listEntity.getIdTask(), listEntity);
        Assert.assertNull(newTask);
    }

    @Test
    void addTask() {
        TaskDTO listEntity = new TaskDTO();
        listEntity.setTaskName("Yerevan");
        listEntity.setIdTask(0);

        when(service.addTask(listEntity)).thenReturn(listEntity);

        TaskDTO newTask = service.addTask(listEntity);
        Assert.assertEquals(listEntity, newTask);
    }

    @Test
    void getTask() {
        TaskDTO listEntity = new TaskDTO();
        listEntity.setTaskName("Yerevan");
        listEntity.setIdTask(0);

        when(service.getTask(listEntity.getIdTask())).thenReturn(listEntity);

        TaskDTO newTask= service.getTask(listEntity.getIdTask());
        Assert.assertEquals(listEntity, newTask);

        TaskDTO newEmptyTask = service.getTask(2);
        Assert.assertNull(newEmptyTask);
    }

    @Test
    void getAllTasks() {
        TaskDTO listEntity = new TaskDTO();
        listEntity.setTaskName("Yerevan");
        List<TaskDTO> list = singletonList(listEntity);

        when(service.getAllTasks()).thenReturn(list);

        List<TaskDTO> newTask= service.getAllTasks();
        Assert.assertEquals(list, newTask);
    }
}