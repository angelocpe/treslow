package com.treslow.mstask.service;

import com.treslow.mstask.entity.TaskEntity;
import com.treslow.mstask.repository.TaskRepository;
import com.treslow.mstask.service.external.TagService;
import com.treslow.tag_public.DTO.TagDTO;
import com.treslow.task_public.TaskDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Service of Task
 */
@Service
public class TaskService {
    @Autowired
    ModelMapper mapper;
    private final TaskRepository  taskRepository;

    @Autowired
    TagService tagService;

    /**
     * Constructor
     * @param taskRepository
     */
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Delete Task given its id
     * @param id
     */
    public void deleteTask(long id) {
        taskRepository.deleteById(id);
    }

    /**
     * Update TaskDTO given its id and its new data (TaskDTO)
     * @param id
     * @param taskDTO
     * @return TaskDTO updated, null if the Task doesn't already exist
     */
    public TaskDTO updateTask(long id, TaskDTO taskDTO) {
        if (id != taskDTO.getIdTask()) {
            return null;
        } else if (!taskRepository.existsById(id)) {
            return null;
        } else {
            TaskEntity task = mapper.map(taskDTO, TaskEntity.class);
            TaskEntity taskBd = taskRepository.save(task);
            return mapper.map(taskBd, TaskDTO.class);
        }
    }
    /**
     * Add TaskDTO given its data (TaskDTO)
     * @param taskDTO
     * @return TaskDTO add
     * */
    public TaskDTO addTask(TaskDTO taskDTO) {
        TaskEntity test = mapper.map(taskDTO, TaskEntity.class);
        TaskEntity task = taskRepository.save(test);
        System.out.println(task);
        TaskDTO testDto = mapper.map(task, TaskDTO.class);
        return testDto;
    }
    /**
     * Get TaskDTO given its id
     * @param id
     * @return TaskDTO, null if it doesn't exist
     */
    public TaskDTO getTask(long id) {
        Optional<TaskEntity> taskEntity = taskRepository.findById(id);
        if (taskEntity.isPresent()) {
            return mapper.map(taskEntity.get(), TaskDTO.class);
        } else {
            return null;
        }
    }
    /**
     * Get all TaskDTO saved in db
     * @return List of TaskDTO
     */
    public List<TaskDTO> getAllTasks() {
        List<TaskEntity> taskEntities = new ArrayList<>();
        taskRepository.findAll().forEach(taskEntities::add);
        return taskEntities
                .stream()
                .map(entry -> mapper.map(entry, TaskDTO.class))
                .collect(Collectors.toList());
    }
    /**
     * Get all Tasks for a certain list of Lists given by their ids
     * @param ids List of ids of Lists
     * @param token Security token
     * @return Map<Long, List<TaskDTO>> map with key = id of list, value = List of related TaskDTO
     */
    public Map<Long, List<TaskDTO>> getTasksByIdLists(List<Long> ids, String token) {

        //Get all Tasks for each List
        List< TaskDTO> taskMap = new ArrayList<>();
        List<TaskEntity> taskEntityList = taskRepository.findAllByIdListIn(ids);
        for(TaskEntity task : taskEntityList){
            taskMap.add( mapper.map(task, TaskDTO.class));
        }
        //Get all Tag for each Task
        List<Long> tasksIdList = new ArrayList<>();
        taskMap.forEach(task -> tasksIdList.add(task.getIdTask()));
        Map<Long, List<TagDTO>> tagMap = tagService.getTagsByIds(tasksIdList, token);

        for(TaskDTO task : taskMap){
            task.setTags(tagMap.get(task.getIdTask()));
        }

        //Map every information together
        Map<Long, List<TaskDTO>> tasksForLists = new HashMap<>();
        for(Long id : ids){
            List<TaskDTO> taskDTOList = new ArrayList<>(taskMap);
            taskDTOList.removeIf(taskDTO -> taskDTO.getIdList() != id);
            tasksForLists.put(id,taskDTOList);
        }

        return tasksForLists;
    }
    /**
     * Get all the Tasks for a certain Sprint given by its id
     * @param id
     * @return List of related TaskDTO for the Sprint
     */
    public List<TaskDTO> getTasksByIdSprint(Long id) {
        List<TaskEntity> taskEntities = taskRepository.findAllByIdSprint(id);
        return taskEntities
                .stream()
                .map(entry -> mapper.map(entry, TaskDTO.class))
                .collect(Collectors.toList());
    }
}
