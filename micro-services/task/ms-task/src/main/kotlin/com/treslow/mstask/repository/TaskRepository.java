package com.treslow.mstask.repository;

import com.treslow.mstask.entity.TaskEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<TaskEntity, Long> {
    List<TaskEntity> findAllByIdListIn(List<Long> idList);

    List<TaskEntity> findAllByIdSprint(Long id);
}
