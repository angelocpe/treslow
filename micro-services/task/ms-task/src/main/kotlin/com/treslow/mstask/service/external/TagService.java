package com.treslow.mstask.service.external;

import com.treslow.tag_public.DTO.TagDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
public class TagService {

    @Autowired
    RestTemplate restTemplate;
    @Value("${tag.api.url}")
    String TAG_API_URL;

    String GET_TAG_BY_TASK_URL = "/tags/tasks";

    public Map<Long, List<TagDTO>> getTagsByIds(List<Long> taskIds, String token) {
        ParameterizedTypeReference<Map<Long, List<TagDTO>>> responseType = new ParameterizedTypeReference<>() {
        };
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("Authorization", "Bearer " + token);
        HttpEntity<List<Long>> httpEntity = new HttpEntity<>(taskIds, httpHeaders);
        ResponseEntity<Map<Long, List<TagDTO>>> responseEntity = restTemplate.exchange(
                TAG_API_URL + GET_TAG_BY_TASK_URL,
                HttpMethod.POST,
                httpEntity,
                responseType);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            return responseEntity.getBody();
        }
        return null;

    }
}
