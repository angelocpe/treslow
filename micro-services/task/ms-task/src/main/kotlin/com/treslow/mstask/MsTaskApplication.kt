package com.treslow.mstask

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import org.modelmapper.ModelMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication

@OpenAPIDefinition(info = Info(title = "Task Rest Api", version = "1.0", description = "Information about the Task APi"))

class MsTaskApplication

fun main(args: Array<String>) {
	runApplication<MsTaskApplication>(*args)
}
