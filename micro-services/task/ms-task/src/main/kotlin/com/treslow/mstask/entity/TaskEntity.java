package com.treslow.mstask.entity;

import net.bytebuddy.dynamic.loading.InjectionClassLoader;

import javax.persistence.*;
import java.util.Date;

@Entity
public class TaskEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long idTask;

    @Column
    private long idList;

    @Column
    private long idSprint;

    @Column
    private String taskName;

    @Column
    private String description;

    @Column
    private float storypoint;

    @Column
    private long assign;

    @Column
    private String type;

    @Column
    private int position;

    @Column
    private int priority;

    @Column
    private long idAuthor;

    @Column
    private Date createDate;

    @Column
    private Date updateDate;

    @Column
    private boolean calculAuto;

    @Column
    private String asA;

    @Column
    private String iWant;

    @Column
    private String soThat;

    public long getIdTask() {
        return idTask;
    }

    public void setIdTask(long idTask) {
        this.idTask = idTask;
    }

    public long getIdList() {
        return idList;
    }

    public void setIdList(long idList) {
        this.idList = idList;
    }

    public long getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(long idSprint) {
        this.idSprint = idSprint;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getStorypoint() {
        return storypoint;
    }

    public void setStorypoint(float storypoint) {
        this.storypoint = storypoint;
    }

    public long getAssign() {
        return assign;
    }

    public void setAssign(long assign) {
        this.assign = assign;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public long getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(long idAuthor) {
        this.idAuthor = idAuthor;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isCalculAuto() {
        return calculAuto;
    }

    public void setCalculAuto(boolean calculAuto) {
        this.calculAuto = calculAuto;
    }

    public String getAsA() {
        return asA;
    }

    public void setAsA(String asA) {
        this.asA = asA;
    }

    public String getiWant() {
        return iWant;
    }

    public void setiWant(String iWant) {
        this.iWant = iWant;
    }

    public String getSoThat() {
        return soThat;
    }

    public void setSoThat(String soThat) {
        this.soThat = soThat;
    }
}
