package com.treslow.mstask.controller;

import com.treslow.mstask.config.Router;
import com.treslow.mstask.service.TaskService;
import com.treslow.task_public.TaskDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;


@RestController
public class TaskController {

    private final static Logger LOG = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    TaskService taskService;
    @RequestMapping(method= RequestMethod.GET, value=Router.API_START +"/tasks")
    public List<TaskDTO> getAllTasks(){
        return taskService.getAllTasks();
    }

    @RequestMapping(method= RequestMethod.GET, value=Router.API_START +"/task/{id}")
    public ResponseEntity<TaskDTO> getTask(@PathVariable long id){
        TaskDTO taskDTO = taskService.getTask(id);
        if (taskDTO != null) {
            return new ResponseEntity<TaskDTO>(taskDTO, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task id:" + id + ", not found", null);
        }
    }
    @RequestMapping(method=RequestMethod.POST,value=Router.API_START +"/task")
    public ResponseEntity<TaskDTO> addTask(@RequestBody TaskDTO taskDTO) {
        TaskDTO task = taskService.addTask(taskDTO);
        if (task != null) {
            return new ResponseEntity<TaskDTO>(task, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Task:" + taskDTO + ", could not be inserted", null);
        }
    }
    @RequestMapping(method=RequestMethod.PUT,value=Router.API_START +"/task/{id}")
    public ResponseEntity<TaskDTO> updateTask(@PathVariable long id, @RequestBody TaskDTO taskDTO) {
        TaskDTO task = taskService.updateTask(id,taskDTO);
        if (task != null) {
            return new ResponseEntity<TaskDTO>(task, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Task:" + taskDTO + ", could not be updated", null);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = Router.API_START + "/task/{id}")
    public void deleteTask(@PathVariable long id) {
        taskService.deleteTask(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = Router.API_START + "/tasks/lists")
    public Map<Long, List<TaskDTO>> getTasksByIdLists(
            @RequestHeader("Authorization") String authorization,
            @RequestBody List<Long> ids
    ) {
        String jwtToken = authorization.substring(7);
        return taskService.getTasksByIdLists(ids, jwtToken);
    }

    @RequestMapping(method = RequestMethod.GET, value = Router.API_START + "/tasks/sprint/{id}")
    public List<TaskDTO> getTasksByIdSprint(
            @PathVariable Long id
    ) {
        return taskService.getTasksByIdSprint(id);
    }

}
