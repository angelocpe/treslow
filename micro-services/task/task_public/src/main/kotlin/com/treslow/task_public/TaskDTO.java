package com.treslow.task_public;

import com.treslow.tag_public.DTO.TagDTO;

import java.util.Date;
import java.util.List;

public class TaskDTO {
    private long idTask;

    private long idList;

    private long idSprint;

    private String taskName;

    private String description;

    private float storypoint;

    private long assign;

    private String type;

    private int position;

    private int priority;

    private long idAuthor;

    private Date createDate;

    private Date updateDate;

    private boolean calculAuto;

    private String asA;

    private String iWant;

    private String soThat;

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }

    private List<TagDTO> tags;

    public long getIdTask() {
        return idTask;
    }

    public void setIdTask(long idTask) {
        this.idTask = idTask;
    }

    public long getIdList() {
        return idList;
    }

    public void setIdList(long idList) {
        this.idList = idList;
    }

    public long getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(long idSprint) {
        this.idSprint = idSprint;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getStorypoint() {
        return storypoint;
    }

    public void setStorypoint(float storypoint) {
        this.storypoint = storypoint;
    }

    public long getAssign() {
        return assign;
    }

    public void setAssign(long assign) {
        this.assign = assign;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public long getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(long idAuthor) {
        this.idAuthor = idAuthor;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isCalculAuto() {
        return calculAuto;
    }

    public void setCalculAuto(boolean calculAuto) {
        this.calculAuto = calculAuto;
    }

    public String getAsA() {
        return asA;
    }

    public void setAsA(String asA) {
        this.asA = asA;
    }

    public String getiWant() {
        return iWant;
    }

    public void setiWant(String iWant) {
        this.iWant = iWant;
    }

    public String getSoThat() {
        return soThat;
    }

    public void setSoThat(String soThat) {
        this.soThat = soThat;
    }
}
