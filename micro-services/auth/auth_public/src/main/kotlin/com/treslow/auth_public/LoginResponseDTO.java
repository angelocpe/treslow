package com.treslow.auth_public;

import com.treslow.user_public.UserDTO;

public class LoginResponseDTO {
    private UserDTO userDTO;
    private String token;

    public LoginResponseDTO() {
    }

    public LoginResponseDTO(UserDTO userDTO, String token) {
        this.userDTO = userDTO;
        this.token = token;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoginResponseDTO{" +
                "userDTO=" + userDTO +
                ", token='" + token + '\'' +
                '}';
    }
}
