package com.treslow.msauth.config

object Router {
    const val API_START = "auth-service"
    const val AUTH_START = "$API_START/auth"
    const val LOGIN_URL = "$AUTH_START/login"
    const val REGISTER_URL = "$AUTH_START/register"
}