package com.treslow.msauth.service

import com.treslow.auth_public.LoginDTO
import com.treslow.auth_public.LoginResponseDTO
import com.treslow.auth_public.RegisterDTO

interface AuthServiceInterface {
    fun login(loginDTO: LoginDTO): LoginResponseDTO
    fun register(registerDTO: RegisterDTO)
}