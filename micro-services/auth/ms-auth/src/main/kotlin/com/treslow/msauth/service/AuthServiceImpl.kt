package com.treslow.msauth.service

import com.treslow.auth_public.LoginDTO
import com.treslow.auth_public.LoginResponseDTO
import com.treslow.auth_public.RegisterDTO
import com.treslow.common.ErrorMessages
import com.treslow.common.exceptions.*
import com.treslow.msauth.security.JwtUtils
import com.treslow.msauth.service.external.UserService
import com.treslow.user_public.UserExistsRequestDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class AuthServiceImpl @Autowired constructor(
    val userService: UserService,
    val passwordEncoder: PasswordEncoder,
    val jwtUtils: JwtUtils
) : AuthServiceInterface
{
    /**
     * Checks user logins and generates a jwt token if they are valid.
     * Returns UserDTO and jwt token.
     */
    override fun login(loginDTO: LoginDTO): LoginResponseDTO {
        val userDto = userService.getUserByUsername(loginDTO.username)
        userDto?.let {
            if (passwordEncoder.matches(loginDTO.password, userDto.password)) {
                return LoginResponseDTO(it, jwtUtils.generateLoggedInJwt(userDto.id))
            } else {
                throw PasswordNotMatchException(ErrorMessages.PasswordNotMatch.message)
            }
        }
        throw UserNotFoundException(ErrorMessages.UserNotFound.message + " username: ${loginDTO.username}")
    }

    /**
     * Register a user.
     * Checks if logins aren't already in use.
     */

    override fun register(registerDTO: RegisterDTO) {
        val userExistsResponseDTO = userService.checkIfUserExists(
            UserExistsRequestDTO(
                registerDTO.username,
                registerDTO.email
            )
        )
        userExistsResponseDTO?.let {
            if (it.isEmailAlreadyUsed) {
                if(it.isUsernameAlreadyUsed) {
                    with(ErrorMessages.UsernameAndEmailAlreadyInUse.message) {
                        throw UsernameAndEmailAlreadyInUseException(
                            this + " username: ${registerDTO.username}" + " email: ${registerDTO.email}"
                        )
                    }

                }
                with(ErrorMessages.EmailAlreadyInUse.message) {
                    throw EmailAlreadyInUseException(this + " email: ${registerDTO.email}")
                }
            } else if(it.isUsernameAlreadyUsed) {
                with(ErrorMessages.UsernameAlreadyInUse.message) {
                    throw UsernameAlreadyInUseException(this + " username: ${registerDTO.username}")
                }
            } else {
                registerDTO.password = passwordEncoder.encode(registerDTO.password)
                userService.registerUser(registerDTO)
            }
        }

    }
}