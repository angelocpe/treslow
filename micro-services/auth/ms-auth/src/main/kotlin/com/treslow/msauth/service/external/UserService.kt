package com.treslow.msauth.service.external

import com.treslow.auth_public.RegisterDTO
import com.treslow.common.ErrorMessages
import com.treslow.common.exceptions.RegistrationFailedException
import com.treslow.msauth.security.JwtUtils
import com.treslow.user_public.UserDTO
import com.treslow.user_public.UserExistsRequestDTO
import com.treslow.user_public.UserExistsResponseDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

/**
 * Service to fetch data from user micro-service
 */
@Service
class UserService
constructor(
    @Value("\${user.api.url}")
    val USER_API_URL: String
) {
    @Autowired
    lateinit var restTemplate: RestTemplate

    @Autowired
    lateinit var jwtUtils: JwtUtils

    var GET_USER_BY_USERNAME_START_URL: String = "$USER_API_URL/user"

    /**
     * Sends a request to fetch a user by his username.
     */
    fun getUserByUsername(username: String): UserDTO? {
        val httpHeaders = HttpHeaders()
        httpHeaders.set("Authorization", "Bearer ${jwtUtils.generateUserServiceMsToken()}")
        val requestEntity: HttpEntity<Void> = HttpEntity(httpHeaders)
        val responseEntity: ResponseEntity<UserDTO> = restTemplate.exchange(
            "$GET_USER_BY_USERNAME_START_URL/$username",
            HttpMethod.GET,
            requestEntity,
            UserDTO::class
        )
        if (responseEntity.statusCode == HttpStatus.OK) {
            return responseEntity.body
        }
        return null
    }

    /**
     * Sends a request to check if user exists.
     */
    fun checkIfUserExists(userExistsRequestDTO: UserExistsRequestDTO): UserExistsResponseDTO? {
        val httpHeaders = HttpHeaders()
        httpHeaders.set("Authorization", "Bearer ${jwtUtils.generateUserServiceMsToken()}")
        val requestEntity: HttpEntity<UserExistsRequestDTO> = HttpEntity(userExistsRequestDTO, httpHeaders)
        val responseEntity: ResponseEntity<UserExistsResponseDTO> = restTemplate.exchange(
            "$USER_API_URL/userexists",
            HttpMethod.POST,
            requestEntity,
            UserExistsResponseDTO::class
        )
        if (responseEntity.statusCode == HttpStatus.OK) {
            return responseEntity.body
        }
        return null
    }

    /**
     * Sends a request to create an account when a user registers
     */
    fun registerUser(registerDTO: RegisterDTO) {
        val httpHeaders = HttpHeaders()
        httpHeaders.set("Authorization", "Bearer ${jwtUtils.generateUserServiceMsToken()}")
        val requestEntity: HttpEntity<RegisterDTO> = HttpEntity(registerDTO, httpHeaders)
        val responseEntity: ResponseEntity<UserDTO> = restTemplate.exchange(
            "$USER_API_URL/register",
            HttpMethod.POST,
            requestEntity,
            UserDTO::class
        )
        if(responseEntity.statusCode == HttpStatus.OK) {

        } else {
            throw RegistrationFailedException(ErrorMessages.RegistrationFailed.message)
        }
    }
}