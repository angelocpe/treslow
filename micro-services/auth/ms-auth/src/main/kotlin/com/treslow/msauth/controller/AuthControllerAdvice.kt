package com.treslow.msauth.controller

import com.treslow.common.ErrorMessages
import com.treslow.common.ErrorResponseDTO
import com.treslow.common.exceptions.*
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@ControllerAdvice
@EnableWebMvc
class AuthControllerAdvice {

    @ResponseBody
    @ExceptionHandler
    fun onEmailAlreadyInUseException(e: EmailAlreadyInUseException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.EmailAlreadyInUse) {
            return response(e.message, status, code)
        }
    }

    @ResponseBody
    @ExceptionHandler
    fun onUsernameAlreadyInUseException(e: UsernameAlreadyInUseException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.UsernameAlreadyInUse) {
            return response(e.message, status, code)
        }
    }

    @ResponseBody
    @ExceptionHandler
    fun onPasswordNotMatchException(e: PasswordNotMatchException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.PasswordNotMatch) {
            return response(e.message, status, code)
        }
    }

    @ResponseBody
    @ExceptionHandler
    fun onUserNotFountException(e: UserNotFoundException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.UserNotFound) {
            return response(e.message, status, code)
        }
    }

    @ResponseBody
    @ExceptionHandler
    fun onUsernameAndEmailAlreadyInUseException(e: UsernameAndEmailAlreadyInUseException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.UsernameAndEmailAlreadyInUse) {
            return response(e.message, status, code)
        }
    }

    @ResponseBody
    @ExceptionHandler
    fun onRegistrationFailedException(e: RegistrationFailedException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.RegistrationFailed) {
            return response(e.message, status, code)
        }
    }

    fun response(message: String?, status: HttpStatus, code: Long): ResponseEntity<ErrorResponseDTO> {
        return ResponseEntity(ErrorResponseDTO(message, status, code), status)
    }
}