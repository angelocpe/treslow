package com.treslow.msauth.security

import com.treslow.auth_public.AuthMSRoles
import io.jsonwebtoken.Jwts
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.security.KeyFactory
import java.security.PrivateKey
import java.security.PublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*


@Component
class JwtUtils(
    @Value("\${private.key}")
    private val privateKeyString: String,
    @Value("\${public.key}")
    private val publicKeyString: String,
    @Value("\${jwt.expiration.ms}")
    private val jwtExpirationMs: Long
) {
    lateinit var privateKey: PrivateKey
    lateinit var publicKey: PublicKey

    init {
        val pkcs8EncodedBytes = Base64.getMimeDecoder().decode(
            privateKeyString
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "")
                .replace("\n", "")
                .trim()
        )
        val privateKeySpec = PKCS8EncodedKeySpec(pkcs8EncodedBytes)
        val privateKeyFactory: KeyFactory = KeyFactory.getInstance("RSA")
        privateKey = privateKeyFactory.generatePrivate(privateKeySpec)

        val x509EncodedBytes = Base64.getMimeDecoder().decode(
            publicKeyString
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("-----END PUBLIC KEY-----", "")
                .replace("\n", "")
                .trim()
        )
        val publicKeySpec = X509EncodedKeySpec(x509EncodedBytes)
        val publicKeyFactory: KeyFactory = KeyFactory.getInstance("RSA")
        publicKey = publicKeyFactory.generatePublic(publicKeySpec)
    }

    fun generateLoggedInJwt(userId: Long): String {
        val date = Date()
        return Jwts.builder()
            .setIssuedAt(date)
            .setExpiration(Date(date.time + jwtExpirationMs))
            .claim("userId", userId)
            .signWith(privateKey)
            .compact()
    }

    fun generateUserServiceMsToken(): String {
        val date = Date()
        return Jwts.builder()
            .setIssuedAt(date)
            .setExpiration(Date(date.time + jwtExpirationMs))
            .claim("role", AuthMSRoles.ROLE_ACCESS_USERS.toString())
            .signWith(privateKey)
            .compact()
    }
}