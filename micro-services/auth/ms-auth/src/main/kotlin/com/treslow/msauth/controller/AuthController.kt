package com.treslow.msauth.controller

import com.treslow.auth_public.LoginDTO
import com.treslow.auth_public.RegisterDTO
import com.treslow.msauth.config.Router
import com.treslow.msauth.service.AuthServiceInterface
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class AuthController
    @Autowired constructor(
        val authService: AuthServiceInterface
    )
{
    @PostMapping(Router.LOGIN_URL)
    fun login(@RequestBody loginDTO: LoginDTO) = authService.login(loginDTO)

    @PostMapping(Router.REGISTER_URL)
    fun register(@RequestBody registerDTO: RegisterDTO) = authService.register(registerDTO)
}