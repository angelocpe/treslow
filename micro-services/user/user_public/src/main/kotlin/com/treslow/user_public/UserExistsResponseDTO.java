package com.treslow.user_public;

public class UserExistsResponseDTO {
    private Boolean isUsernameAlreadyUsed;
    private Boolean isEmailAlreadyUsed;

    public UserExistsResponseDTO() {
    }

    public UserExistsResponseDTO(Boolean isUsernameAlreadyUsed, Boolean isEmailAlreadyUsed) {
        this.isUsernameAlreadyUsed = isUsernameAlreadyUsed;
        this.isEmailAlreadyUsed = isEmailAlreadyUsed;
    }

    public Boolean getIsUsernameAlreadyUsed() {
        return isUsernameAlreadyUsed;
    }

    public void setIsUsernameAlreadyUsed(Boolean isUsernameAlreadyUsed) {
        this.isUsernameAlreadyUsed = isUsernameAlreadyUsed;
    }

    public Boolean getIsEmailAlreadyUsed() {
        return isEmailAlreadyUsed;
    }

    public void setIsEmailAlreadyUsed(Boolean isEmailAlreadyUsed) {
        this.isEmailAlreadyUsed = isEmailAlreadyUsed;
    }

    @Override
    public String toString() {
        return "com.treslow.user_public.UserExistsResponseDTO{" +
                "username=" + isUsernameAlreadyUsed +
                ", email=" + isEmailAlreadyUsed +
                '}';
    }
}
