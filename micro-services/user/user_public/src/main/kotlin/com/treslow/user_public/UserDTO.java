package com.treslow.user_public;

public class UserDTO {
    private Long id;
    private String username;
    private String email;
    private String password;
    private Theme theme;
    private Boolean isGoogle;

    public UserDTO(Long id, String username, String email, String password, Theme theme, Boolean isGoogle) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.theme = theme;
        this.isGoogle = isGoogle;
    }

    public UserDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public Boolean getIsGoogle() {
        return isGoogle;
    }

    public void setIsGoogle(Boolean isGoogle) {
        this.isGoogle = isGoogle;
    }

    @Override
    public String toString() {
        return "com.treslow.user_public.UserDTO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", theme=" + theme +
                ", isGoogle=" + isGoogle +
                '}';
    }
}
