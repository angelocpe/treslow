package com.treslow.user_public;

public class UserExistsRequestDTO {
    private String username;
    private String email = null;

    public UserExistsRequestDTO() {

    }

    public UserExistsRequestDTO(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "com.treslow.user_public.UserExistsRequestDTO{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
