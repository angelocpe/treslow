package com.treslow.msuser.entity

import com.treslow.user_public.Theme
import org.hibernate.validator.constraints.Length
import javax.persistence.*

@Entity
data class UserEntity (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    @Column(unique = true)
    val username: String? = null,
    val email: String? = null,
    @Length(min = 12, max = 128)
    val password: String? = null,
    val theme: Theme,
    val isGoogle: Boolean,
)