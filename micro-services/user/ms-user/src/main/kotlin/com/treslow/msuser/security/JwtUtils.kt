package com.treslow.msuser.security

import com.treslow.auth_public.AuthMSRoles
import io.jsonwebtoken.Jwts
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.json.GsonJsonParser
import org.springframework.stereotype.Component
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec
import java.util.*


@Component
class JwtUtils(
    @Value("\${auth.public.key}")
    private val authPublicKeyString: String,
) {
    lateinit var authPublicKey: PublicKey

    init {
        val x509EncodedBytes = Base64.getMimeDecoder().decode(
            authPublicKeyString
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("-----END PUBLIC KEY-----", "")
                .replace("\n", "")
                .trim()
        )
        val publicKeySpec = X509EncodedKeySpec(x509EncodedBytes)
        val publicKeyFactory: KeyFactory = KeyFactory.getInstance("RSA")
        authPublicKey = publicKeyFactory.generatePublic(publicKeySpec)
    }

    fun verifyTokenValidity(token: String): Boolean {
        Jwts.parserBuilder().setSigningKey(authPublicKey).build().parseClaimsJws(token)
        return true
    }

    fun verifyTokenFromAuthServer(token: String): Boolean {
        return (Jwts
            .parserBuilder()
            .setSigningKey(authPublicKey)
            .build()
            .parseClaimsJws(token)
            .body["role"].toString()) == AuthMSRoles.ROLE_ACCESS_USERS.toString()
    }
}