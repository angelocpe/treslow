package com.treslow.msuser.controller

import com.treslow.auth_public.RegisterDTO
import com.treslow.user_public.UserExistsRequestDTO
import com.treslow.msuser.config.Router
import com.treslow.msuser.service.UserServiceInterface
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController @Autowired constructor(
    val userService: UserServiceInterface
) {
    @GetMapping(Router.USERS_URL)
    fun getAllUsers() =
        userService.getAllUsers()

    @PostMapping(Router.USERS_IDS_URL)
    fun getAllUserMapByIds(@RequestBody ids: List<Long>) = userService.getAllUsersMapByIds(ids)

    @GetMapping(Router.USER_URL)
    fun getUserByUsername(@PathVariable username: String) =
        userService.getUserByUsername(username)

    @PostMapping(Router.USER_EXISTS_URL)
    fun postUserExists(@RequestBody userExistsRequestDTO: UserExistsRequestDTO) =
        userService.userExists(userExistsRequestDTO)

    @PostMapping(Router.USER_REGISTER_URL)
    fun postRegister(@RequestBody registerDTO: RegisterDTO) =
        userService.registerUser(registerDTO)
}