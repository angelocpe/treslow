package com.treslow.msuser.security

import com.treslow.common.ErrorMessages
import com.treslow.common.exceptions.UnauthorizedException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.util.StringUtils
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Configuration
class AuthTokenFilter @Autowired constructor(private val jwtUtils: JwtUtils): OncePerRequestFilter() {

    val authServerAuthorizedUrls = arrayListOf<String>("/api/userexists", "/api/register", "/api/user/")

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        try {
            val jwt = parseJwt(request)
            val requestUri = request.requestURI
            var requestFromServer = false
            for(uri in authServerAuthorizedUrls) {
                if(requestUri.startsWith(uri)) {
                    if(jwtUtils.verifyTokenFromAuthServer(jwt)) {
                        requestFromServer = true
                        break
                    } else {
                        with(ErrorMessages.Unauthorized) {
                            response.sendError(status.value(), message)
                        }
                    }
                }
            }
            if(!requestFromServer) {
                if(jwtUtils.verifyTokenValidity(jwt)) {
                } else {
                    with(ErrorMessages.Unauthorized) {
                        response.sendError(status.value(), message)
                    }
                }
            }
        } catch (e: UnauthorizedException) {
            response.sendError(ErrorMessages.Unauthorized.status.value(), e.message)
        }
        filterChain.doFilter(request, response)
    }

    private fun parseJwt(request: HttpServletRequest): String {
        val headerAuth = request.getHeader("Authorization")
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7, headerAuth.length)
        } else throw UnauthorizedException(ErrorMessages.Unauthorized.message)
    }
}