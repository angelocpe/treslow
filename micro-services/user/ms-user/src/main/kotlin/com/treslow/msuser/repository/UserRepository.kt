package com.treslow.msuser.repository

import com.treslow.msuser.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository: JpaRepository<UserEntity, Long> {
    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM UserEntity u where u.username = :username")
    fun existsByUsername(@Param("username") username: String): Boolean

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM UserEntity u where u.email = :email")
    fun existsByEmail(@Param("email") email: String): Boolean

    @Query("SELECT u from UserEntity u where u.username = :username")
    fun findUserByUsername(@Param("username") username: String): Optional<UserEntity>
}