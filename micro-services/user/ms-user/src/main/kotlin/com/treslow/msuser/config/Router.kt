package com.treslow.msuser.config

object Router {
    const val API_START = "user-service"
    const val USERS_URL = "$API_START/users"
    const val USERS_IDS_URL = "$API_START/users_ids"
    const val USER_URL = "$API_START/user/{username}"
    const val USER_EXISTS_URL = "$API_START/userexists"
    const val USER_REGISTER_URL = "$API_START/register"
}