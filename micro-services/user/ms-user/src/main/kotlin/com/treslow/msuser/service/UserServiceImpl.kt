package com.treslow.msuser.service

import com.treslow.auth_public.RegisterDTO
import com.treslow.user_public.Theme
import com.treslow.user_public.UserDTO
import com.treslow.user_public.UserExistsRequestDTO
import com.treslow.user_public.UserExistsResponseDTO
import com.treslow.msuser.entity.UserEntity
import com.treslow.msuser.mapper.toDTO
import com.treslow.msuser.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl
    @Autowired constructor(
        val userRepository: UserRepository
    )
: UserServiceInterface {
    override fun getAllUsers(): List<UserDTO> {
        return userRepository.findAll().map { userEntity -> userEntity.toDTO() }
    }

    override fun getAllUsersMapByIds(ids: List<Long>): Map<Long, UserDTO> {
        val usersMap = HashMap<Long, UserDTO>()
        userRepository.findAllById(ids).map { userEntity -> usersMap.put(userEntity.id, userEntity.toDTO()) }
        return usersMap
    }

    override fun getUserByUsername(username: String): UserDTO? {
        if(userRepository.existsByUsername(username)) {
              return userRepository.findUserByUsername(username).get().toDTO()
        }
        return null
    }

    override fun userExists(userExistsRequestDTO: UserExistsRequestDTO): UserExistsResponseDTO {
        val response = UserExistsResponseDTO(false, false)
        if(userRepository.existsByUsername(userExistsRequestDTO.username)) {
            response.isUsernameAlreadyUsed = true
        }
        userExistsRequestDTO.email?.let {
            if(it.isNotBlank() && userRepository.existsByEmail(userExistsRequestDTO.email)) {
                response.isEmailAlreadyUsed = true
            }
        }
        return response
    }

    override fun registerUser(registerDTO: RegisterDTO): UserDTO {
        val userEntity = UserEntity(0, registerDTO.username, registerDTO.email, registerDTO.password, Theme.LIGHT, false)
        return userRepository.save(userEntity).toDTO()
    }

    override fun deleteAccount(username: String): Boolean {
        TODO("Not yet implemented")
    }

}