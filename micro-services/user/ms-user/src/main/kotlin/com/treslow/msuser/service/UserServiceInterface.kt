package com.treslow.msuser.service

import com.treslow.auth_public.RegisterDTO
import com.treslow.user_public.UserDTO
import com.treslow.user_public.UserExistsRequestDTO
import com.treslow.user_public.UserExistsResponseDTO

interface UserServiceInterface {
    /**
     * Get all Users stored in db
     * @return list of UserDTO
     */
    fun getAllUsers(): List<UserDTO>

    /**
     * Get all Users given their ids
     * @return map of key = userId, value = UserDTO
     */
    fun getAllUsersMapByIds(ids: List<Long>): Map<Long, UserDTO>

    /**
     * Get User given its username
     * @return UserDTO
     */
    fun getUserByUsername(username: String): UserDTO?

    /**
     * Check if User exists given its email and username
     * @return UserExistsResponseDTO
     */
    fun userExists(userExistsRequestDTO: UserExistsRequestDTO): UserExistsResponseDTO

    /**
     * Save User in db
     * @return UserDTO (from db)
     */
    fun registerUser(registerDTO: RegisterDTO): UserDTO

    /**
     * Delete User given its username
     */
    fun deleteAccount(username: String): Boolean
}