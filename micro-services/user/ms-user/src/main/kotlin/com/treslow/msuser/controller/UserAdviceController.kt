package com.treslow.msuser.controller

import com.treslow.common.ErrorMessages
import com.treslow.common.ErrorResponseDTO
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.UnsupportedJwtException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@ControllerAdvice
@EnableWebMvc
class UserAdviceController {

    @ResponseBody
    @ExceptionHandler
    fun onMalformedJwtException(e: MalformedJwtException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.MalformedJwt) {
            return response(e.message, status, code)
        }
    }

    @ResponseBody
    @ExceptionHandler
    fun onExpiredJwtException(e: ExpiredJwtException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.ExpiredJwt) {
            return response(e.message, status, code)
        }
    }

    @ResponseBody
    @ExceptionHandler
    fun onUnsupportedJwtException(e: UnsupportedJwtException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.UnsupportedJwt) {
            return response(e.message, status, code)
        }
    }

    @ResponseBody
    @ExceptionHandler
    fun onIllegalArgumentException(e: IllegalArgumentException): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.IllegalArgument) {
            return response(e.message, status, code)
        }
    }

    @ResponseBody
    @ExceptionHandler
    fun onException(e: Exception): ResponseEntity<ErrorResponseDTO> {
        with(ErrorMessages.Exception) {
            return response(e.message, status, code)
        }
    }

    fun response(message: String?, status: HttpStatus, code: Long): ResponseEntity<ErrorResponseDTO> {
        return ResponseEntity(ErrorResponseDTO(message, status, code), status)
    }
}