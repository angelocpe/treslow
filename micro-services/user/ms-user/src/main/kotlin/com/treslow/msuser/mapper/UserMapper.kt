package com.treslow.msuser.mapper

import com.treslow.user_public.UserDTO
import com.treslow.msuser.entity.UserEntity

fun UserDTO.toEntity() = UserEntity(id, username, email, password, theme, isGoogle)
fun UserEntity.toDTO() =
    UserDTO(id, username, email, password, theme, isGoogle)