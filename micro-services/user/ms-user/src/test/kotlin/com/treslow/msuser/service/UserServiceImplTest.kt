package com.treslow.msuser.service

import com.treslow.msuser.repository.UserRepository
import com.treslow.user_public.UserDTO
import com.treslow.user_public.UserExistsRequestDTO
import com.treslow.user_public.UserExistsResponseDTO
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
internal class UserServiceImplTest {
    @MockBean(name = "userService")
    private val service: UserServiceInterface? = null

    @MockBean(name = "userRepository")
    private val repository: UserRepository? = null

    @Test
    fun getAllUsers() {
        val entity = UserDTO()
        entity.username= "Yerevan"
        val list: List<UserDTO> = listOf<UserDTO>(entity)

        Mockito.`when`<List<UserDTO>>(service!!.getAllUsers()).thenReturn(list)

        val newTask: List<UserDTO> = service.getAllUsers()
        org.junit.Assert.assertEquals(list, newTask)
    }

    @Test
    fun getAllUsersMapByIds() {
        val entity = UserDTO()
        entity.username= "Yerevan"
        entity.id=1
        val list: List<Long> = listOf<Long>(1)
        val map: Map<Long, UserDTO> = mapOf(Pair(1, entity))

        Mockito.`when`<Map<Long, UserDTO>>(service!!.getAllUsersMapByIds(list)).thenReturn(map)

        val newTask: Map<Long, UserDTO> = service.getAllUsersMapByIds(list)
        org.junit.Assert.assertEquals(map, newTask)
    }

    @Test
    fun getUserByUsername() {
        val entity = UserDTO()
        entity.username= "Yerevan"

        Mockito.`when`<UserDTO>(service!!.getUserByUsername("Yerevan")).thenReturn(entity)

        val newTask: UserDTO? = service.getUserByUsername("Yerevan")
        org.junit.Assert.assertEquals(entity, newTask)
    }

    @Test
    fun userExists() {
        val entity = UserExistsRequestDTO()
        entity.username= "Yerevan"
        val response = UserExistsResponseDTO()
        response.isEmailAlreadyUsed = false
        response.isUsernameAlreadyUsed = false

        Mockito.`when`<UserExistsResponseDTO>(service!!.userExists(entity)).thenReturn(response)

        val newTask: UserExistsResponseDTO = service.userExists(entity)
        org.junit.Assert.assertEquals(response, newTask)
    }

    @Test
    fun registerUser() {
    }

    @Test
    fun deleteAccount() {
    }

    @Test
    fun getUserRepository() {
    }
}