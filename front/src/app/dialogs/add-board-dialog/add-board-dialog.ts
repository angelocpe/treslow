import { Component } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { map, Observable, startWith } from 'rxjs';
import { BoardDTO } from 'src/DTO/boardDTO';
import { BoarduserRole, RoleDTO } from 'src/DTO/roleDTO';
import { Theme, UserDTO } from 'src/DTO/userDTO';
import { userRoleDTO as UserRoleDTO } from 'src/DTO/userRoleDTO';
import { BoardService } from 'src/Services/api/board.service';
import { RoleService } from 'src/Services/api/role.service';
import { UserService } from 'src/Services/api/user.service';
import { string } from 'src/string/string';

@Component({
  selector: 'add-board-dialog',
  templateUrl: 'add-board-dialog.html',
  styleUrls: ['add-board-dialog.css'],
})
export class AddBoardDialog {
  string = string;

  users: UserDTO[] = [];
  roles: RoleDTO[] = [];

  filteredUsers: Observable<UserDTO[]>;

  userSearch = new FormControl<string | UserDTO>('');
  roleSelection = new FormControl<RoleDTO | null>(null);
  boardName = new FormControl<string>('', Validators.required);
  background = new FormControl<string>('', Validators.required);

  selectionUserRolesArray: Array<RoleDTO[]> = [];
  addedUsers: UserDTO[] = [];
  addedUserRoles: UserRoleDTO[] = [];

  isUserSelected: boolean = true;
  isUserAlreadyAdded: boolean = false;
  isRoleSelected: boolean = true;

  isRequestBeingSent = false;

  isFormValid = false;

  roleIsMissingMessage = 'Role is missing.';
  userIsMissingMessage = 'User is missing.';

  constructor(
    public dialogRef: MatDialogRef<AddBoardDialog>,
    private userService: UserService,
    private roleService: RoleService,
    private boardService: BoardService
  ) {
    this.roleService.getAllRoles().subscribe((roles) => {
      this.roles = roles.map((role) => new RoleDTO(role.id, role.name));
    });
    this.userService.getAllUsers().subscribe((users) => {
      this.users = users.map(
        (user) =>
          new UserDTO(
            user.id,
            user.username,
            user.email,
            user.pwd,
            user.theme,
            user.isGoogle
          )
      );
    });
    this.filteredUsers = this.userSearch.valueChanges.pipe(
      startWith(''),
      map((value) => {
        const name = typeof value === 'string' ? value : value?.username;
        return name ? this._filter(name) : this.users.slice();
      })
    );

    this.boardName.valueChanges.subscribe((value) => {
      this.isFormValid = this.boardName.valid && this.background.valid;
    });
  }

  addBoard(): void {
    if (!this.isRequestBeingSent) {
      this.isRequestBeingSent = true;
      const board = new BoardDTO(
        0,
        this.boardName.value as string,
        this.background.value as string,
        0,
        this.addedUserRoles,
        []
      );
      this.boardService.postBoard(board).subscribe(
        (response) => {
          console.log('ajouté le board: ', response);
          this.isRequestBeingSent = false;
          this.dialogRef.close();
        },
        (error) => {
          console.log("erreur lors de l'ajout du board: ", error);
          this.isRequestBeingSent = false;
        }
      );
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addUserRole(): void {
    this.isRoleSelected = true;
    this.isUserSelected = true;
    this.isUserAlreadyAdded = false;
    let roleDTO: RoleDTO;
    let userDTO: UserDTO;
    if (this.roleSelection.value instanceof RoleDTO) {
      roleDTO = this.roleSelection.value;
    } else {
      this.isRoleSelected = false;
    }

    if (this.userSearch.value instanceof UserDTO) {
      if (!this.addedUsers.includes(this.userSearch.value)) {
        userDTO = this.userSearch.value;
        this.addedUsers.push(userDTO);
      } else {
        this.isUserAlreadyAdded = true;
      }
    } else {
      this.isUserSelected = false;
    }

    if (
      this.isRoleSelected &&
      this.isUserSelected &&
      !this.isUserAlreadyAdded
    ) {
      this.addedUserRoles.push(new UserRoleDTO(userDTO!!, roleDTO!!));
      this.selectionUserRolesArray.push(this.roles);
    }
  }

  deleteUserRoleByIndex(index: number) {
    this.selectionUserRolesArray.splice(index, 1);
    this.addedUserRoles.splice(index, 1);
    this.addedUsers.splice(index, 1);
  }

  displayFn(user: UserDTO): string {
    return user && user.username ? user.username : '';
  }

  private _filter(name: string): UserDTO[] {
    const filterValue = name.toLowerCase();

    return this.users.filter((user) =>
      user.username.toLowerCase().includes(filterValue)
    );
  }
}
