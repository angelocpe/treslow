import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { map, Observable, startWith } from 'rxjs';
import { TagDTO } from 'src/DTO/tagDTO';
import { TaskDTO } from 'src/DTO/taskDTO';
import { UserDTO } from 'src/DTO/userDTO';
import { userRoleDTO } from 'src/DTO/userRoleDTO';
import { BigDataService } from 'src/Services/api/big-data.service';
import { BoardService } from 'src/Services/api/board.service';
import { TagService } from 'src/Services/api/tag.service';
import { TaskService } from 'src/Services/api/task.service';
import { UserService } from 'src/Services/api/user.service';
import { ConfirmPredictionDialogComponent } from '../confirm-prediction-dialog/confirm-prediction-dialog.component';

@Component({
  selector: 'app-add-task-dialog',
  templateUrl: './add-task-dialog.component.html',
  styleUrls: ['./add-task-dialog.component.css'],
})
export class AddTaskDialogComponent {
  users: UserDTO[] = [];
  filteredUsers: Observable<UserDTO[]>;
  userSearch = new FormControl<string | UserDTO>('');
  addedUsers: UserDTO[] = [];
  tags: TagDTO[] = [];
  filteredTags: Observable<TagDTO[]>;
  tagSearch = new FormControl<string | TagDTO>('');
  addedTags: TagDTO[] = [];
  task: TaskDTO | null = null;

  taskName = new FormControl<string>('');
  description = new FormControl<string>('');
  storypoint = new FormControl<number | null>(null);
  type = new FormControl<string>('');
  priority = new FormControl<number | null>(null);
  asA = new FormControl<string>('');
  iWant = new FormControl<string>('');
  soThat = new FormControl<string>('');

  constructor(
    public dialogRef: MatDialogRef<AddTaskDialogComponent>,
    private tagService: TagService,
    private taskService: TaskService,
    private bigDataService: BigDataService,
    private boardService: BoardService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    boardService.currentBoard.subscribe((board) => {
      if (board) {
        let users = board.userRoles.map((userRoleDTO) => userRoleDTO.userDTO);
        this.users = users.map(
          (user) =>
            new UserDTO(
              user.id,
              user.username,
              user.email,
              user.pwd,
              user.theme,
              user.isGoogle
            )
        );
      }
    });
    this.filteredUsers = this.userSearch.valueChanges.pipe(
      startWith(''),
      map((value) => {
        const name = typeof value === 'string' ? value : value?.username;
        return name ? this._filterUser(name) : this.users.slice();
      })
    );
    tagService
      .getAllTags()
      .subscribe(
        (tags) =>
          (this.tags = tags.map(
            (tag) => new TagDTO(tag.idTag, tag.tagName, tag.color)
          ))
      );
    this.filteredTags = this.tagSearch.valueChanges.pipe(
      startWith(''),
      map((value) => {
        const name = typeof value === 'string' ? value : value?.tagName;
        return name ? this._filterTag(name) : this.tags.slice();
      })
    );
  }

  addTask(): void {
    const task = new TaskDTO(
      0,
      this.data.idList,
      0,
      this.taskName.value ?? '',
      this.description.value ?? '',
      this.storypoint.value ?? 0,
      this.addedUsers[0]?.id ?? 0,
      this.addedTags,
      this.type.value ?? '',
      this.data.postion,
      this.priority.value ?? 0,
      0,
      new Date(),
      new Date(),
      false,
      this.asA.value ?? '',
      this.iWant.value ?? '',
      this.soThat.value ?? ''
    );

    const userStory = `as a ${this.asA} I want ${this.iWant} so that ${this.soThat}`;

    this.bigDataService
      .postSentence(userStory)
      .subscribe((predictionString) => {
        let predictionNumber = Math.round(Number(predictionString));
        this.dialog
          .open(ConfirmPredictionDialogComponent, {
            width: '30vw',
            data: { prediction: predictionNumber },
          })
          .afterClosed()
          .subscribe((validation) => {
            if (validation) task.storypoint = predictionNumber;
            this.taskService.postTask(task).subscribe(
              (response) => {
                this.dialogRef.close();
              },
              (error) => {
                console.log('failed to add task because :', error);
              }
            );
          });
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addUser(): void {
    if (this.userSearch.value instanceof UserDTO) {
      if (!this.addedUsers.includes(this.userSearch.value)) {
        this.addedUsers.push(this.userSearch.value);
      }
    }
  }

  addTag(): void {
    if (this.tagSearch.value instanceof TagDTO) {
      if (!this.addedTags.includes(this.tagSearch.value)) {
        this.addedTags.push(this.tagSearch.value);
      }
    }
  }

  displayFnUser(user: UserDTO): string {
    return user && user.username ? user.username : '';
  }

  displayFnTag(tag: TagDTO): string {
    return tag && tag.tagName ? tag.tagName : '';
  }

  private _filterUser(name: string): UserDTO[] {
    const filterValue = name.toLowerCase();

    return this.users.filter((user) =>
      user.username.toLowerCase().includes(filterValue)
    );
  }

  private _filterTag(name: string): TagDTO[] {
    const filterValue = name.toLowerCase();

    return this.tags.filter((tag) =>
      tag.tagName.toLowerCase().includes(filterValue)
    );
  }
}
