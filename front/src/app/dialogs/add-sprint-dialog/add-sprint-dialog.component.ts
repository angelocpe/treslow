import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-sprint-dialog',
  templateUrl: './add-sprint-dialog.component.html',
  styleUrls: ['./add-sprint-dialog.component.css'],
})
export class AddSprintDialogComponent {
  sprintName = new FormControl<string>('');

  constructor(public dialogRef: MatDialogRef<AddSprintDialogComponent>) {}

  addSprint(): void {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
