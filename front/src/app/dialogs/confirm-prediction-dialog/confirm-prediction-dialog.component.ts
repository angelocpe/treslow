import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface ConfirmPredictionData {
  prediction: number;
}

@Component({
  selector: 'confirm-prediction-dialog',
  templateUrl: './confirm-prediction-dialog.component.html',
  styleUrls: ['./confirm-prediction-dialog.component.css'],
})
export class ConfirmPredictionDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<ConfirmPredictionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmPredictionData,
  
  ) {
  }

  acceptPrediction(): void {
    this.dialogRef.close(true);
  }

  rejectPrediction(): void {
    this.dialogRef.close(false);
  }
}
