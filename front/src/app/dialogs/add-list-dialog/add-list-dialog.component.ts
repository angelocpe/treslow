import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ListDTO } from 'src/DTO/listDTO';
import { BoardService } from 'src/Services/api/board.service';
import { ListService } from 'src/Services/api/list.service';

@Component({
  selector: 'app-add-list-dialog',
  templateUrl: './add-list-dialog.component.html',
  styleUrls: ['./add-list-dialog.component.css'],
})
export class AddListDialogComponent {
  listName = new FormControl<string>('', Validators.required);
  idBoard: number | null;

  isRequestBeingSent = false;

  constructor(
    public dialogRef: MatDialogRef<AddListDialogComponent>,
    private listService: ListService,
    private boardService: BoardService
  ) {
    this.boardService.currentsBoardId.subscribe(boardId => this.idBoard = boardId);
  }

  addList(): void {
    if(this.listName.value !== null && this.idBoard !== null) {
      if(!this.isRequestBeingSent) {
        this.isRequestBeingSent = true;
        this.listService.postList(new ListDTO(0, this.listName.value!!, this.idBoard!!, 1, [])).subscribe(
          (response) => {
            console.log('added list:', response);
            this.isRequestBeingSent = false;
            this.dialogRef.close();
          },
          (error) => {
            console.log('failed to add list because :', error);
            this.isRequestBeingSent = false;
          }
        );
      }
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
