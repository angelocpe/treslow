import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserDTO } from 'src/DTO/userDTO';
import { BoardService } from 'src/Services/api/board.service';
import { UserService } from 'src/Services/api/user.service';
import { TokenService } from 'src/Services/other/token.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  username = 'Username';
  subscription: Subscription;
  user: UserDTO | null = null;

  constructor(
    private userService: UserService,
    protected router: Router,
    private tokenService: TokenService,
    private boardService: BoardService
  ) {
    this.subscription = this.userService.currentUser.subscribe((u) => {
      this.user = u;
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  logout() {
    this.tokenService.changeToken('');
    this.userService.changeUser(null);
    this.userService.changeIsLog(false);
    this.boardService.logout();
    this.router.navigateByUrl('/');
  }
}
