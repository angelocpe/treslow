import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { response } from 'express';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'front';
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private http: HttpClient,
    protected router: Router
  ) {
    this.matIconRegistry.addSvgIcon(
      'info-solid',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/info-solid.svg'
      )
    );
  }

  ngOnInit(): void {
    /*const body = {
      question: 'How you doing ?',
      identifier: 'treslowId',
      data: {
        custom: 'Mood Poll Data',
      },
      options: [
        {
          text: 'Good',
          data: {
            custom: 'data',
          },
        },
        {
          text: 'Bad',
          data: {
            custom: 'data',
          },
        },
        {
          text: 'Meh',
          data: {
            custom: 'data',
          },
        },
      ],
    };

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('api-key', 'PQH2MB4AHS4K0GG28H6R11VJYQJE');

    this.http
      .post('https://api.pollsapi.com/v1/create/poll', body, {
        headers: headers,
      })
      .subscribe((response) => {
        console.log(response);
      });*/
  }
}
