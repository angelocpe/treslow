import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './common/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { AddBoardDialog } from './dialogs/add-board-dialog/add-board-dialog';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { MatDividerModule } from '@angular/material/divider';
import { AuthTokenInterceptor } from 'src/Services/interceptors/AuthTokenInterceptor';
import { BoardComponent } from './components/board/board.component';
import { ListComponent } from './components/list/list.component';
import { TaskComponent } from './components/task/task.component';
import { AddListDialogComponent } from './dialogs/add-list-dialog/add-list-dialog.component';
import { AddTaskDialogComponent } from './dialogs/add-task-dialog/add-task-dialog.component';
import { AddSprintDialogComponent } from './dialogs/add-sprint-dialog/add-sprint-dialog.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MoodApiComponent } from './components/mood-api/mood-api.component';
import { ConfirmPredictionDialogComponent } from './dialogs/confirm-prediction-dialog/confirm-prediction-dialog.component';

@NgModule({
  entryComponents: [AddBoardDialog, AddListDialogComponent, AddSprintDialogComponent, AddSprintDialogComponent, ConfirmPredictionDialogComponent],
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    PageNotFoundComponent,
    AddBoardDialog,
    SignInComponent,
    SignUpComponent,
    BoardComponent,
    ListComponent,
    TaskComponent,
    AddListDialogComponent,
    AddTaskDialogComponent,
    AddSprintDialogComponent,
    MoodApiComponent,
    ConfirmPredictionDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    NgbModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    MatSelectModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDividerModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthTokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
