import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { AddListDialogComponent } from 'src/app/dialogs/add-list-dialog/add-list-dialog.component';
import { AddSprintDialogComponent } from 'src/app/dialogs/add-sprint-dialog/add-sprint-dialog.component';
import { AddTaskDialogComponent } from 'src/app/dialogs/add-task-dialog/add-task-dialog.component';
import { BoardDTO } from 'src/DTO/boardDTO';
import { TagDTO } from 'src/DTO/tagDTO';
import { TaskDTO } from 'src/DTO/taskDTO';
import { BoardService } from 'src/Services/api/board.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
})
export class BoardComponent {
  subscription: Subscription;
  board: BoardDTO | null = null;
  tasks: TaskDTO[] = [];
  tags: TagDTO[] = [];

  constructor(
    private boardService: BoardService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public dialog: MatDialog
  ) {
    this.subscription = this.boardService.currentBoard.subscribe((b) => {
      this.board = b;
    });

    this.matIconRegistry.addSvgIcon(
      'square-plus-solid',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/square-plus-solid.svg'
      )
    );
  }

  ngOnInit() {
    this.boardService.changeBOB(true);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  openAddListDialog(): void {
    this.dialog.open(AddListDialogComponent, {
      width: '30vw',
    });
  }

  openAddSprintDialog(): void {
    this.dialog.open(AddSprintDialogComponent, {
      width: '30vw',
    });
  }
}
