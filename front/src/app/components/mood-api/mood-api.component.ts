import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { MoodPollDTO, PollOptionDTO } from 'src/DTO/moodPoll';
import { UserService } from '../../../Services/api/user.service';

@Component({
  selector: 'app-mood-api',
  templateUrl: './mood-api.component.html',
  styleUrls: ['./mood-api.component.css'],
})
export class MoodApiComponent {
  headers = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('api-key', 'PQH2MB4AHS4K0GG28H6R11VJYQJE');
  username: string | undefined;

  interval: any;
  moodPoll: MoodPollDTO;
  countVoteBad: number;
  countVoteMeh: number;
  countVoteGood: number;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private http: HttpClient,
    private userService: UserService
  ) {
    this.interval = setInterval(() => {
      this.http
        .get<MoodPollDTO>('https://api.pollsapi.com/v1/get/polls', {
          headers: this.headers,
        })
        .subscribe((response) => {
          this.moodPoll = response;
          const votes = this.moodPoll.data.docs[0].options;
          this.countVoteBad = votes.find(this.isBad)?.votes_count ?? 0;
          this.countVoteMeh = votes.find(this.isMeh)?.votes_count ?? 0;
          this.countVoteGood = votes.find(this.isGood)?.votes_count ?? 0;
        });
    }, 1000);

    this.matIconRegistry.addSvgIcon(
      'face-frown-regular',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/face-frown-regular.svg'
      )
    );
    this.matIconRegistry.addSvgIcon(
      'face-meh-regular',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/face-meh-regular.svg'
      )
    );
    this.matIconRegistry.addSvgIcon(
      'face-smile-regular',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/face-smile-regular.svg'
      )
    );
    this.userService.currentUser.subscribe((user) => {
      this.username = user?.username;
    });
  }

  voteSad() {
    let body = {
      poll_id: '63c86d1525e6ed0010f880fa',
      option_id: '63c86d1525e6ed0010f880fc',
      identifier: this.username,
    };

    this.http
      .post('https://api.pollsapi.com/v1/create/vote', body, {
        headers: this.headers,
      })
      .subscribe((resp) => {
        console.log(resp);
      });
  }

  voteMeh() {
    let body = {
      poll_id: '63c86d1525e6ed0010f880fa',
      option_id: '63c86d1525e6ed0010f880fd',
      identifier: this.username,
    };

    this.http
      .post('https://api.pollsapi.com/v1/create/vote', body, {
        headers: this.headers,
      })
      .subscribe((resp) => {
        console.log(resp);
      });
  }

  voteHappy() {
    let body = {
      poll_id: '63c86d1525e6ed0010f880fa',
      option_id: '63c86d1525e6ed0010f880fb',
      identifier: this.username,
    };

    this.http
      .post('https://api.pollsapi.com/v1/create/vote', body, {
        headers: this.headers,
      })
      .subscribe((resp) => {
        console.log(resp);
      });
  }

  isBad(option: PollOptionDTO) {
    return option.text === 'Bad';
  }

  isMeh(option: PollOptionDTO) {
    return option.text === 'Meh';
  }

  isGood(option: PollOptionDTO) {
    return option.text === 'Good';
  }
}
