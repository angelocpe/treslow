import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoodApiComponent } from './mood-api.component';

describe('MoodApiComponent', () => {
  let component: MoodApiComponent;
  let fixture: ComponentFixture<MoodApiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoodApiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MoodApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
