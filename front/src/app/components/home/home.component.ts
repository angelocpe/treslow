import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';
import { AddBoardDialog } from 'src/app/dialogs/add-board-dialog/add-board-dialog';
import { BoardService } from 'src/Services/api/board.service';
import { BoardDTO } from 'src/DTO/boardDTO';
import { Router } from '@angular/router';
import { AddSprintDialogComponent } from 'src/app/dialogs/add-sprint-dialog/add-sprint-dialog.component';

export interface DialogData {
  boardName: string;
  background: string;
  idAuthor: number;
  users: string[];
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  boards: BoardDTO[] | undefined;
  background: string | undefined;
  idAuthor: number | undefined;
  users: string[] | undefined;
  board: BoardDTO | null = null;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public dialog: MatDialog,
    private boardService: BoardService,
    protected router: Router
  ) {
    boardService.currentsBoards.subscribe((boards) => (this.boards = boards));

    this.matIconRegistry.addSvgIcon(
      'square-plus-solid',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/square-plus-solid.svg'
      )
    );

    this.matIconRegistry.addSvgIcon(
      'trash-solid',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/trash-solid.svg'
      )
    );

    this.boardService.changeBOB(false);
  }

  openDialog(): void {
    this.dialog.open(AddBoardDialog, {
      width: '30vw',
    });
  }

  onBoard(board: BoardDTO) {
    this.boardService.changeBoard(board);
    this.boardService.changeBoardId(board.idBoard);
    this.router.navigateByUrl('/board');
  }
}
