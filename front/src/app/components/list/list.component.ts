import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddTaskDialogComponent } from 'src/app/dialogs/add-task-dialog/add-task-dialog.component';
import { ListDTO } from 'src/DTO/listDTO';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent {
  @Input() list: ListDTO;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    this.dialog.open(AddTaskDialogComponent, {
      width: '30vw',
      data: { idList: this.list.idList, position: 1 },
    });
  }
}
