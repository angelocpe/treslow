import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginDTO } from 'src/DTO/loginDTO';
import { UserService } from 'src/Services/api/user.service';
import { TokenService } from 'src/Services/other/token.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent {
  hide = true;

  usernameFormControl = new FormControl('', [Validators.required]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(12),
  ]);

  constructor(
    private userService: UserService,
    protected router: Router,
    private tokenService: TokenService
  ) {}

  onLogin() {
    const user = new LoginDTO(
      this.usernameFormControl.value!,
      this.passwordFormControl.value!
    );
    this.userService.login(user).subscribe(
      (response) => {
        this.tokenService.changeToken(response.token);
        this.userService.changeUser(response.userDTO);
        this.router.navigateByUrl('/home');
      },
      (error) => {
        console.log('erreur');
      }
    );
  }
}
