import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterDTO } from 'src/DTO/registerDTO';
import { UserService } from 'src/Services/api/user.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent {
  constructor(
    private http: HttpClient,
    private userService: UserService,
    protected router: Router
  ) {}

  hide = true;

  usernameFormControl = new FormControl('', [Validators.required]);

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(
      /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_-]).{12,}/
    ),
  ]);

  onRegister() {
    const email = this.emailFormControl.value ?? null;
    const user = new RegisterDTO(
      this.usernameFormControl.value!,
      email,
      this.passwordFormControl.value!
    );
    this.userService.register(user).subscribe(
      (response) => {
        console.log('registered');
        this.router.navigateByUrl('/sign-in');
      },
      (error) => {
        console.log('erreur');
      }
    );
  }
}
