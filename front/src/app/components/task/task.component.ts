import { Component, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { TaskDTO } from 'src/DTO/taskDTO';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
})
export class TaskComponent {
  @Input() task: TaskDTO;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      'poker',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../assets/poker.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'high-priority',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        '../../assets/high-priority.svg'
      )
    );
  }
}
