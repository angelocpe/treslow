import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserService } from '../api/user.service';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  private tokenSource = new BehaviorSubject<string | null>(null);
  currentToken = this.tokenSource.asObservable();

  constructor(private userService: UserService) {}

  changeToken(token: string) {
    this.tokenSource.next(token);
    this.userService.changeIsLog(true);
  }
}
