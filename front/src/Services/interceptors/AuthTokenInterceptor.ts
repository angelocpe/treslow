import { Injectable, OnDestroy } from '@angular/core';
import {
  HttpInterceptor,
  HttpEvent,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TokenService } from '../other/token.service';

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor, OnDestroy {
  tokenSubscription: Subscription;
  token: string | null;
  constructor(private tokenService: TokenService) {
    this.token = null;
    this.tokenSubscription = this.tokenService.currentToken.subscribe(
      (token) => (this.token = token)
    );
  }

  NO_AUTH_ENDPOINTS = [environment.LOGIN_URL, environment.REGISTER_URL];

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!this.NO_AUTH_ENDPOINTS.includes(req.url)) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${this.token}`,
        },
      });
    }
    return next.handle(req);
  }

  ngOnDestroy(): void {
    this.tokenSubscription.unsubscribe();
  }
}
