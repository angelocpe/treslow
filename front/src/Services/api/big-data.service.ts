import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BigDataService {

  constructor(private http: HttpClient) { }

  postSentence(sentence: string): Observable<string> {
    return this.http.post<string>(environment.BIG_DATA_URL, {sentence: sentence});
  }
}
