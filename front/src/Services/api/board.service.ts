import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { BoardDTO } from 'src/DTO/boardDTO';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class BoardService {
  private boardSource = new BehaviorSubject<BoardDTO | null>(null);
  currentBoard = this.boardSource.asObservable();

  private boardsSource = new BehaviorSubject<BoardDTO[]>([]);
  currentsBoards = this.boardsSource.asObservable();

  private boardOrBoardsSource = new BehaviorSubject<boolean>(false);
  currentsBoardOrBoards = this.boardOrBoardsSource.asObservable();

  private boardIdSource = new BehaviorSubject<number | null>(null);
  currentsBoardId = this.boardIdSource.asObservable();

  intervalBoards: any;
  intervalBoard: any;

  constructor(private http: HttpClient, private userService: UserService) {
    this.currentsBoardOrBoards.subscribe((bOB) => {
      clearInterval(this.intervalBoard);
      if (bOB === false) {
        this.userService.currentIsLog.subscribe((isLog) => {
          if (isLog === true) {
            this.intervalBoards = setInterval(() => {
              this.getAllBoards().subscribe((boards) =>
                this.boardsSource.next(boards)
              );
            }, 1000);
          }
        });
      } else {
        this.userService.currentIsLog.subscribe((isLog) => {
          clearInterval(this.intervalBoards);
          if (isLog === true) {
            this.intervalBoard = setInterval(() => {
              this.currentsBoardId.subscribe((boardId) => {
                this.getBoard(boardId!).subscribe((board) =>
                  this.boardSource.next(board)
                );
                console.log('Board : ', this.currentBoard);
              });
            }, 1000);
          }
        });
      }
    });
  }

  changeBoard(board: BoardDTO) {
    this.boardSource.next(board);
  }

  changeBoardId(boardId: number) {
    this.boardIdSource.next(boardId);
  }

  changeBOB(bOB: boolean) {
    this.boardOrBoardsSource.next(bOB);
  }

  getAllBoards(): Observable<BoardDTO[]> {
    return this.http.get<BoardDTO[]>(environment.BOARDS_API_URL);
  }

  getBoard(idBoard: number): Observable<BoardDTO> {
    return this.http.get<BoardDTO>(environment.BOARD_API_URL + '/' + idBoard);
  }

  postBoard(board: BoardDTO): Observable<BoardDTO[]> {
    return this.http.post<BoardDTO[]>(environment.BOARDS_API_URL, board);
  }

  logout() {
    clearInterval(this.intervalBoard);
    clearInterval(this.intervalBoards);
    this.boardSource.next(null);
    this.boardsSource.next([]);
  }
}
