import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ListDTO } from 'src/DTO/listDTO';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ListService {
  constructor(private http: HttpClient) {}

  postList(list: ListDTO): Observable<ListDTO[]> {
    return this.http.post<ListDTO[]>(environment.LIST_API_URL, list);
  }
}
