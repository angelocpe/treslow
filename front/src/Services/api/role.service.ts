import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { RoleDTO } from 'src/DTO/roleDTO';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class RoleService {
  rolesSource = new BehaviorSubject<RoleDTO[]>([]);
  currentroles = this.rolesSource.asObservable();

  interval: any;

  constructor(private http: HttpClient, private userService: UserService) {
    this.userService.currentIsLog.subscribe((isLog) => {
      if (isLog === true) {
        this.interval = setInterval(() => {
          this.getAllRoles().subscribe((roles) => this.rolesSource.next(roles));
        }, 1000);
      }
    });
  }

  getAllRoles(): Observable<RoleDTO[]> {
    return this.http.get<RoleDTO[]>(environment.ROLE_API_URL);
  }
}
