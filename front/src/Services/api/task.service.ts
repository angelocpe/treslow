import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskDTO } from 'src/DTO/taskDTO';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  constructor(private http: HttpClient) {}

  postTask(task: TaskDTO): Observable<TaskDTO[]> {
    return this.http.post<TaskDTO[]>(environment.TASK_API_URL, task);
  }
}
