import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { TagDTO } from 'src/DTO/tagDTO';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class TagService {
  private tagsSource = new BehaviorSubject<TagDTO[]>([]);
  currentsTags = this.tagsSource.asObservable();

  interval: any;

  constructor(private http: HttpClient, private userService: UserService) {
    this.userService.currentIsLog.subscribe((isLog) => {
      if (isLog === true) {
        this.interval = setInterval(() => {
          this.getAllTags().subscribe((tags) => this.tagsSource.next(tags));
        }, 1000);
      }
    });
  }

  getAllTags(): Observable<TagDTO[]> {
    return this.http.get<TagDTO[]>(environment.TAGS_API_URL);
  }
}
