import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SprintDTO } from 'src/DTO/sprintDTO';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SprintService {
  constructor(private http: HttpClient) {}

  postSprint(sprint: SprintDTO): Observable<SprintDTO[]> {
    return this.http.post<SprintDTO[]>(environment.SPRINT_API_URL, sprint);
  }
}
