import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserDTO } from 'src/DTO/userDTO';
import { RegisterDTO } from 'src/DTO/registerDTO';
import { environment } from 'src/environments/environment';
import { LoginDTO } from 'src/DTO/loginDTO';
import { LoginResponseDTO } from 'src/DTO/loginResponseDTO';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  USERS: UserDTO[] = [];

  private userSource = new BehaviorSubject<UserDTO | null>(null);
  currentUser = this.userSource.asObservable();

  private isLogSource = new BehaviorSubject<Boolean>(false);
  currentIsLog = this.isLogSource.asObservable();

  constructor(private http: HttpClient) {}

  changeUser(user: UserDTO | null) {
    this.userSource.next(user);
  }

  changeIsLog(isLog: Boolean) {
    this.isLogSource.next(isLog);
  }

  getAllUsers(): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(environment.USERS_API_URL);
  }

  register(user: RegisterDTO): Observable<void> {
    return this.http.post<void>(environment.REGISTER_URL, user);
  }

  login(user: LoginDTO): Observable<LoginResponseDTO> {
    return this.http.post<LoginResponseDTO>(environment.LOGIN_URL, user);
  }
}
