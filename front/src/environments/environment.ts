const HOST_START = 'http://localhost:80';
const AUTH_START = `${HOST_START}/auth-service/auth`;
const USER_START = `${HOST_START}/user-service`;
const BOARD_START = `${HOST_START}/board-service`;
const LIST_START = `${HOST_START}/list-service`;
const TASK_START = `${HOST_START}/task-service`;
const TAG_START = `${HOST_START}/tag-service`;
const ROLE_START = `${HOST_START}/role-service`;
const SPRINT_START = `${HOST_START}/sprint-service`;
const BIG_DATA_START = `${HOST_START}/big-data-service`;

export const environment = {
  // AUTH ENDPOINTS
  LOGIN_URL: `${AUTH_START}/login`,
  REGISTER_URL: `${AUTH_START}/register`,

  // USER_ENDPOINTS
  USERS_API_URL: `${USER_START}/users`,

  // BOARDS ENDPOINTS
  BOARDS_API_URL: `${BOARD_START}/boards`,
  BOARD_API_URL: `${BOARD_START}/board`,

  // LIST ENDPOINTS
  LIST_API_URL: `${LIST_START}/list`,

  // TASK ENDPOINTS
  TASK_API_URL: `${TASK_START}/task`,

  // TAG ENDPOINTS
  TAGS_API_URL: `${TAG_START}/tags`,
  TAG_API_URL: `${TAG_START}/tag`,

  // ROLE ENDPOINTS
  ROLE_API_URL: `${ROLE_START}/roles`,

  // SPRINT ENDPOINTS
  SPRINTS_API_URL: `${SPRINT_START}/sprints`,
  SPRINT_API_URL: `${SPRINT_START}/sprint`,

  // BIG DATA ENDPOINTS
  BIG_DATA_URL: `${BIG_DATA_START}/prediction`
};
