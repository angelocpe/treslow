export const string = {
    ROLE_ADMINISTRATOR: 'Administrator',
    ROLE_MODERATOR: 'Moderator',
    ROLE_USER: 'User',
    ROLE_VIEWER: 'Viewer',
}