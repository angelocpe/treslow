import { UserDTO } from './userDTO';

export class LoginResponseDTO {
  token: string;
  userDTO: UserDTO;

  constructor(token: string, userDTO: UserDTO) {
    this.token = token;
    this.userDTO = userDTO;
  }
}
