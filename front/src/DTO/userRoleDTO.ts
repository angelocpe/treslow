import { RoleDTO } from './roleDTO';
import { UserDTO } from './userDTO';

export class userRoleDTO {
  userDTO: UserDTO;
  roleDTO: RoleDTO;
  constructor(userDTO: UserDTO, roleDTO: RoleDTO) {
    this.userDTO = userDTO;
    this.roleDTO = roleDTO;
  }
}
