export class RegisterDTO {
  username: string;
  email: string | null;
  password: string;

  constructor(username: string, email: string | null, password: string) {
    this.username = username;
    this.email = email;
    this.password = password;
  }
}
