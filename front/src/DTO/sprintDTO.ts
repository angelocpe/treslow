export class SprintDTO {
  idSprint: number;
  sprintName: string;
  startDate: Date;
  endDate: Date;
  constructor(
    idSprint: number,
    sprintName: string,
    startDate: Date,
    endDate: Date
  ) {
    this.idSprint = idSprint;
    this.sprintName = sprintName;
    this.startDate = startDate;
    this.endDate = endDate;
  }
}
