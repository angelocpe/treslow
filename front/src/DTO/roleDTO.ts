export enum BoarduserRole {
  ROLE_ADMINISTRATOR = 'ROLE_ADMINISTRATOR',
  ROLE_MODERATOR = 'ROLE_MODERATOR',
  ROLE_USER = 'ROLE_USER',
  ROLE_VIEWER = 'ROLE_VIEWER',
}

export class RoleDTO {
  id: number;
  name: BoarduserRole;

  constructor(id: number, name: BoarduserRole) {
    this.id = id;
    this.name = name;
  }
}
