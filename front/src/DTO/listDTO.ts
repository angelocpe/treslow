import { TaskDTO } from './taskDTO';

export class ListDTO {
  idList: number;
  listName: string;
  idBoard: number;
  position: number;
  tasks: TaskDTO[];

  constructor(
    idList: number,
    listName: string,
    idBoard: number,
    position: number,
    tasks: TaskDTO[]
  ) {
    this.idList = idList;
    this.listName = listName;
    this.idBoard = idBoard;
    this.position = position;
    this.tasks = tasks;
  }
}
