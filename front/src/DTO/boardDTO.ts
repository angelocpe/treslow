import { ListDTO } from './listDTO';
import { userRoleDTO } from './userRoleDTO';

export class BoardDTO {
  idBoard: number;
  boardName: string;
  background: string;
  idAuthor: number;
  userRoles: userRoleDTO[];
  lists: ListDTO[];

  constructor(
    idBoard: number,
    boardName: string,
    background: string,
    idAuthor: number,
    usersRoles: userRoleDTO[],
    lists: ListDTO[]
  ) {
    this.idBoard = idBoard;
    this.boardName = boardName;
    this.background = background;
    this.idAuthor = idAuthor;
    this.userRoles = usersRoles;
    this.lists = lists;
  }
}
