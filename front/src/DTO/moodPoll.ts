export class MoodPollDTO {
  data: PollDataDTO;

  constructor(data: PollDataDTO) {
    this.data = data;
  }
}

export class PollDataDTO {
  docs: PollDocDTO[];

  constructor(docs: PollDocDTO[]) {
    this.docs = docs;
  }
}

export class PollDocDTO {
  id: string;
  options: PollOptionDTO[];

  constructor(id: string, options: PollOptionDTO[]) {
    this.id = id;
    this.options = options;
  }
}

export class PollOptionDTO {
  text: string;
  votes_count: number;

  constructor(text: string, votes_count: number) {
    this.text = text;
    this.votes_count = votes_count;
  }
}
