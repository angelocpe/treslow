import { TagDTO } from './tagDTO';
import { UserDTO } from './userDTO';

export class TaskDTO {
  idTask: number;
  idList: number;
  idSprint: number;
  taskName: string;
  description: string;
  storypoint: number;
  assign: number;
  tags: TagDTO[];
  type: string;
  position: number;
  priority: number;
  idAuthor: number;
  createDate: Date;
  updateDate: Date;
  calculAuto: boolean;
  asA: string;
  iWant: string;
  soThat: string;

  constructor(
    idTask: number,
    idList: number,
    idSprint: number,
    taskName: string,
    description: string,
    storypoint: number,
    assign: number,
    tags: TagDTO[],
    type: string,
    position: number,
    priority: number,
    idAuthor: number,
    createDate: Date,
    updateDate: Date,
    calculAuto: boolean,
    asA: string,
    iWant: string,
    soThat: string
  ) {
    this.idTask = idTask;
    this.idList = idList;
    this.idSprint = idSprint;
    this.taskName = taskName;
    this.description = description;
    this.storypoint = storypoint;
    this.assign = assign;
    this.tags = tags;
    this.type = type;
    this.position = position;
    this.priority = priority;
    this.idAuthor = idAuthor;
    this.createDate = createDate;
    this.updateDate = updateDate;
    this.calculAuto = calculAuto;
    this.asA = asA;
    this.iWant = iWant;
    this.soThat = soThat;
  }
}
