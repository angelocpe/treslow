export enum Theme {
  DARK,
  LIGHT,
}

export class UserDTO {
  id: number;
  username: string;
  email: string | null;
  pwd: string;
  theme: Theme;
  isGoogle: boolean;

  constructor(
    id: number,
    username: string,
    email: string | null,
    pwd: string,
    theme: Theme,
    isGoogle: boolean
  ) {
    this.id = id;
    this.username = username;
    this.email = email;
    this.pwd = pwd;
    this.theme = theme;
    this.isGoogle = isGoogle;
  }
}
