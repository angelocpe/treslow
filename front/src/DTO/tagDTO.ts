export class TagDTO {
  idTag: number;
  tagName: string;
  color: string;

  constructor(idTag: number, tagName: string, color: string) {
    this.idTag = idTag;
    this.tagName = tagName;
    this.color = color;
  }
}
